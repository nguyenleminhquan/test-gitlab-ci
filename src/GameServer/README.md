### Những bash scripts ở folder `src/GameServer` dùng để setup môi trường live ở máy dev nếu có nhu cầu.
### Yêu cầu:
Đã có Docker và Docker Compose trên máy.
- [Link tải Docker](https://docs.docker.com/engine/install/) 
- [Link tải Docker Compose v1.29.2](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04)
### Step chạy
- Chỉnh sửa lại biến `COUCHBASE_USERNAME` và `COUCHBASE_PASSWORD` ở file `scripts/Couchbase/init.sh` để tạo Couchbase.
- Nếu chạy lần đầu thì dùng script `install.sh` sẽ tải những tool cần thiết và run Couchbase cùng server.
- Nếu server và Couchbase đã chạy, có thay đổi code gì và muốn chạy server với code mới thì chạy script `update.sh`.