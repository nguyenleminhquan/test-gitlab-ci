#!/bin/bash

# Running this script will run master server, event server together with Couchbase and Redis
# Make sure you have downloaded docker-volume-local-persist
systemctl status docker-volume-local-persist 1>/dev/null
_result=$?
if [ "$_result" != "0" ]; then
  echo "Please download docker-volume-local-persist before running this script!"
  echo "See: https://github.com/MatchbookLab/local-persist"
  exit 1
fi

source ./scripts/Couchbase/init.sh
source ./scripts/Redis/init.sh

docker network create $LGR_NETWORK

# Run Couchbase
docker-compose -p $COMPOSE_PROJECT_NAME -f $LGR_DB_COMPOSE up -d
sleep 10
docker exec $LGR_DB_NAME /bin/bash /app/run-couchbase.sh

# Run Redis
if [ ! -d "$LGR_REDIS_CONF/" ]; then
  mkdir -p $LGR_REDIS_CONF
  touch $LGR_REDIS_CONF/redis.conf
  cp -f scripts/Redis/redis.conf $LGR_REDIS_CONF/redis.conf
fi
docker-compose -p $COMPOSE_PROJECT_NAME -f $LGR_REDIS_COMPOSE up -d

# Run master server
bash ./up.sh $LGR_SERVER_COMPOSE
# Run event server
bash ./up.sh $LGR_EVENT_SERVER_COMPOSE 