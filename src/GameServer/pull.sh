#!/bin/bash

source ./init.sh $1

docker image pull $LGR_SERVER_IMAGE:$LGR_SERVER_TAG
docker image pull $LGR_EVENT_SERVER_IMAGE:$LGR_SERVER_TAG