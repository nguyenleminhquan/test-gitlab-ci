#!/bin/bash

source ./init.sh

# Build master server
docker build -t $LGR_SERVER_IMAGE:$LGR_SERVER_TAG_LOCAL --build-arg ENV=SERVER_LIVE .
# Build event server
docker build -t $LGR_EVENT_SERVER_IMAGE:$LGR_SERVER_TAG_LOCAL --build-arg ENV=SERVER_LIVE .