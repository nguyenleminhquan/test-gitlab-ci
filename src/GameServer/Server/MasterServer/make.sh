#!/bin/bash

source ./config.sh $1

echo "Check has lib"
HAS_LIB=0
if [ -n $LIBRARY ]; then
  HAS_LIB=1
fi

echo "Clean folder"
if [ -d "$DIR_TEMP" ]; then
  rm -rf "$DIR_TEMP"
fi
if [ -d "$DIR_TEMP_SRC" ]; then
  rm -rf "$DIR_TEMP_SRC"
fi
if [ -d "$DIR_TEMP_CLASS" ]; then 
  rm -rf "$DIR_TEMP_CLASS"
fi
if [ -d "$DIR_RELEASE" ]; then 
  rm -rf "$DIR_RELEASE"
fi
if [ -d "$DIR_RELEASE_LIB" ]; then 
  rm -rf "$DIR_RELEASE_LIB"
fi
if [ ! -d "$DIR_RELEASE_DATA" ]; then 
  rm -rf "$DIR_RELEASE_DATA"
fi

echo "Create folder"
mkdir "$DIR_TEMP"
mkdir "$DIR_TEMP_SRC"
mkdir "$DIR_TEMP_CLASS"
mkdir "$DIR_RELEASE"
mkdir "$DIR_RELEASE_LIB"
mkdir "$DIR_RELEASE_DATA"

# echo "Export protocols"
# source ./export.sh $1

echo "Copy libs"
ALL_LIBS=$(find $DIR_LIB -type f -name '*.jar')
cp $ALL_LIBS $DIR_RELEASE_LIB

echo "Copy config"
cp -rf $DIR_INFO/$ENVIRONMENT/. $DIR_RELEASE_DATA/

echo "Copy cmd runtime"
cd $PRJ_HOME
cp -f run-on-docker.sh $DIR_RELEASE

echo "Copy sources to temp dir"
cp -rf $DIR_SRC/. $DIR_TEMP_SRC/

echo "Compile sources"
find $DIR_TEMP_SRC -type f -name '*.java' > $DIR_TEMP/listjava.txt
cd $DIR_TEMP
$JAVAC -Xlint:unchecked -Xlint:deprecation -encoding UTF-8 -classpath $LIBRARY -d "$DIR_TEMP_CLASS" @listjava.txt

echo "Create MANIFEST.MF"
cd $DIR_TEMP
echo "Build-Version: $BUILD_VERSION" >> MANIFEST.MF
echo "Build-Time: $(date)" >> MANIFEST.MF
echo "Build-User: $(whoami)" >> MANIFEST.MF
echo "Build-Computer: $(cat /proc/sys/kernel/hostname)" >> MANIFEST.MF 
echo "Main-Class: $MAIN_CLASS" >> MANIFEST.MF
if [ "$HAS_LIB" -eq "1" ]; then
  COUNT=0
  CLASS_PATH=Class-Path:
  for lib in $(find $DIR_LIB -type f -name '*.jar')
  do
    LIB_NAME=$(basename $lib)
    if [ "$COUNT" -eq "0" ]; then
      echo "$CLASS_PATH lib/$LIB_NAME" >> MANIFEST.MF
    else
      echo "  lib/$LIB_NAME" >> MANIFEST.MF
    fi
    ((COUNT+=1))
  done
  cat MANIFEST.MF
fi

echo "Make jar"
cd $DIR_TEMP_CLASS
$JAR -cmf ../MANIFEST.MF $DIR_RELEASE/$JAR_NAME .