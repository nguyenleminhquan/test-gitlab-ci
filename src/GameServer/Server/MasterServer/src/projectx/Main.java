package projectx;

import projectx.database.*;
import projectx.iap.IAPManager;
import projectx.io.GameServerInitializer;
import firebat.io.http.HttpServer;
import firebat.io.udp.Udp;
import firebat.utils.*;
import firebat.db.memcached.BucketManager;
import firebat.log.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.File;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;



public class Main {
    private final static ServiceStatus status = new ServiceStatus(true);
    public static Environment environment;
    public static HttpServer httpServer;
	
    public static void main (String[] args) {
        try {
            status.setStartingUp();
            addShutdownHook();
			int listenPort = 8081;
			String env = "";
			for(int i = 0; i < args.length; i++)
			{
				if(args[i].equals("-port"))
					listenPort = Integer.parseInt(args[i+1]);
				if(args[i].equals("-environment"))
					env = args[i+1];
			}
			if(env == "")
				environment = Environment.DEV;
			else
			{
				if(env.equals("SERVER_LIVE"))
					environment = Environment.SERVER_LIVE;
				else
					environment = Environment.DEV;
			}
            Log.start(environment);
            BucketManager.start(environment);
			Database.init();
            IAPManager.init();
			
            httpServer = new HttpServer();
            httpServer.start("*", listenPort, new GameServerInitializer());
			status.setStarted();

            System.out.println("["+String.valueOf(environment)+"] UTC Time : >>> " + Misc.getCurrentUtcTime());
            Log.debug("Start service...");
        } catch (Exception e) {
            Log.exception(e);
        }
    }

    public static synchronized void stop() throws Exception {
        status.setShuttingDown();
        if (httpServer != null)
            httpServer.stop();
		BucketManager.stop();
        Log.stop();
        status.setShutdown();
    }

    private static void addShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    Main.stop();
                } catch (Exception e) {
                    Log.exception(e);
                }
            }
        });
    }

    public static boolean isRunning () {
        return status.isStarted();
    }
}





