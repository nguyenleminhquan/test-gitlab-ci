package projectx.utils;

public class Const {
    public final static String PROJX_BUNDLE_ID = "com.viking.tycoon.idle.game";
    public final static String ANDROID_PUBLIC_KEY_STORE = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApP2KGhybQATM7l0HVkWQSptjayF8X+LezTPCIznl1mZpmY77ccHx5t0LnYpe+uz/IgQs+e/3XL/VJB+RuerfDZQE+Td9ipXV7zm7ZAIt1FWuzyz6iR2c+FAyYOk3VO5r3H5Th1F+Hiz9ZJ88EhoQdxgnfo3DusaFKEZGBVqFvjv/yK0HAVpEmBiD962LzbUq6DwTnRVwh27y4cuMgB3JIkfHkDG8X3y1TZYB/9y2MNOYjY18rV3OdaVmfD6ViqdHddKZuLbFKnMdrAo/7Dqx31Ud+hNwngyQrUIfxYPQkX14MwRD3enjgFnHkwO6m1L4Kqm0T9sWR9QmWFcpbMsHewIDAQAB";
    public final static String ANDROID_APPSFLYER_URL = "https://api2.appsflyer.com/inappevent/com.viking.tycoon.idle.game";
    public final static String IOS_APPSFLYER_URL = "insert_url_here";
    public final static String APPSFLYER_DEV_KEY = "FU3nVd2iNSvXKECF8kj8Qd";
}