package projectx.utils;

import com.google.protobuf.*;
import projectx.database.*;
import firebat.utils.*;
import firebat.db.memcached.Document;
import firebat.log.*;
import com.couchbase.client.java.json.JsonObject;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

public class ProjXHelper {
    
    // save/load data from database
    public static boolean setObjToDB(String objKey, byte[] objData) {
        return setObjToDB(objKey, objData, false);
    }
    public static boolean setObjToDB(String objKey, byte[] objData, boolean isPlaceOnMemCache) {
        // if(isPlaceOnMemCache) {
        //     return Database.BUCKET_CACHE.set(objKey, objData);
        // } 
        // return Database.BUCKET_BASE.set(objKey, objData);
        return false;
    }
    public static byte[] getObjFromDB(String objKey) {
        return getObjFromDB(objKey, false);
    }
    public static byte[] getObjFromDB(String objKey, boolean isPlaceOnMemCache) {
        // if(isPlaceOnMemCache) {
        //     return (byte[])Database.BUCKET_CACHE.get(objKey);
        // } 
        // return (byte[])Database.BUCKET_BASE.get(objKey);
        return null;
    }
    public static boolean setDocumentToDB(String key, Document document) {
        return setDocumentToDB(key, document, false);
    }
    
    public static boolean setDocumentToDB(String key, Document document, boolean isPlaceOnMemCache) {
        if(isPlaceOnMemCache) {
            return Database.BUCKET_CACHE.set(key, document);
        }
        else {
            return Database.BUCKET_BASE.set(key, document);
        }
    }

    public static boolean replaceDocumentInDB(String key, Document document) {
        return replaceDocumentInDB(key, document, false);
    }

    public static boolean replaceDocumentInDB(String key, Document document, boolean isPlaceOnMemCache) {
        if (isPlaceOnMemCache) {
            return Database.BUCKET_CACHE.replace(key, document);
        } else {
            return Database.BUCKET_BASE.replace(key, document);
        }
    }

    public static <T extends Document> T getDocumentFromDB(Class<T> type, String key) {
        return getDocumentFromDB(type, key, false);
    }

    public static <T extends Document> T getDocumentFromDB(Class<T> type, String key, boolean isPlaceOnMemCache) {
        if(isPlaceOnMemCache) {
            return Database.BUCKET_CACHE.get(type, key);
        }
        else {
            return Database.BUCKET_BASE.get(type, key);
        }
    }
    
    public static boolean deleteDocumentInDB(String key) {
        return deleteDocumentInDB(key, false);
    }

    public static boolean deleteDocumentInDB(String key, boolean isPlaceOnMemCache) {
        if (isPlaceOnMemCache) {
            return Database.BUCKET_CACHE.delete(key);
        } else {
            return Database.BUCKET_BASE.delete(key);
        }
    }

    // protocol buffer
    public static byte[] byteStringToByteArray(ByteString inputString) {
        return inputString.toByteArray();
    }
    public static ByteString byteArrayToByteString(byte[] byteArray) {
        return ByteString.copyFrom(byteArray);
    }
    
    // verify iap receipt
    public static boolean verifyReceiptAndroid(String signature, String jsonData) throws Exception {
        return AndroidSign.verifyPurchase(signature, jsonData, Const.ANDROID_PUBLIC_KEY_STORE);
    }
    public static String verifyReceiptIos(String receipt, boolean isEnvDev) throws Exception {
        return AppleSign.verifyApplePurchase(receipt, isEnvDev);
    }
    
    // write log ifrs
    public static void writeLogIFRS(String userId
                                        , String platform
                                        , String skuId
                                        , String tranPrice
                                        , String tranCurrency) {
        float fPrice = 0;
        try {
            fPrice = Float.parseFloat(tranPrice);
        } catch (NumberFormatException e) {
            fPrice = 0;
        }
        if(!tranCurrency.equals("VND") && fPrice > 0) {
            Log.ifrs(userId, Misc.getCrtServerTimeInUnix(), fPrice, skuId, convertPlatform4Log(platform), fPrice, tranCurrency);
        }
    }
    // sending log s2s
    public static void sendLogPurchaseToAppsflyer(boolean isAndroid          
                                                            , String bundleVersion
                                                            , String appsflyerId
                                                            , String transValue
                                                            , String transCurrency
                                                            , String skuId
                                                            , String deviceIp
                                                            , String orderId
                                                            , String sessionId
                                                            , String card
                                                            , String elixir
                                                            , String gem
                                                            , String land
                                                            , String eventId) throws Exception {
        String url = "";
        String body = "";
        if(isAndroid) {
            url = Const.ANDROID_APPSFLYER_URL;
            body = genAppsflyerPurchaseBodyAndroid(bundleVersion
                                                    , appsflyerId
                                                    , "na"
                                                    , transValue
                                                    , transCurrency
                                                    , skuId
                                                    , deviceIp
                                                    , orderId
                                                    , sessionId
                                                    , card
                                                    , elixir
                                                    , gem
                                                    , land
                                                    , eventId);
        } else {
            url = Const.IOS_APPSFLYER_URL;
            body = genAppsflyerPurchaseBodyIos(bundleVersion
                                                    , appsflyerId
                                                    , "na"
                                                    , transValue
                                                    , transCurrency
                                                    , skuId
                                                    , deviceIp
                                                    , orderId
                                                    , sessionId
                                                    , card
                                                    , elixir
                                                    , gem
                                                    , land
                                                    , eventId);
        }
        
        Misc.sendingPostRequestAsync(url, body , Const.APPSFLYER_DEV_KEY);
    }
    public static String genAppsflyerPurchaseBodyAndroid(String clientBundleVersion
                                                            , String appsflyerId
                                                            , String idfa
                                                            , String localPrice
                                                            , String localCurrency
                                                            , String skuId
                                                            , String deviceIp
                                                            , String orderId
                                                            , String sessionId
                                                            , String card
                                                            , String elixir
                                                            , String gem 
                                                            , String land
                                                            , String eventId) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		
        JsonObject jsonValue = JsonObject.create()
                                .put("af_client_version", clientBundleVersion)
                                .put("Session ID", sessionId)
                                .put("af_sku_id", skuId)
                                .put("Card", card)
                                .put("Elixir", elixir)
                                .put("Gem", gem)
                                .put("Land", land)
                                .put("af_order_id", orderId)
                                .put("af_revenue", localPrice)
                                .put("af_currency", localCurrency)
                                .put("EventID", eventId);
        String evtValueStr = jsonValue.toString();
		ObjectNode objRootNode = mapper.createObjectNode();
		objRootNode.put("appsflyer_id", appsflyerId);
        objRootNode.put("advertising_id", idfa);
        objRootNode.put("eventName", "af_purchase");
        objRootNode.put("eventValue", evtValueStr);
        objRootNode.put("eventCurrency", localCurrency);
        objRootNode.put("ip", deviceIp);
        objRootNode.put("eventTime", Misc.getCurrentUtcTime());
        objRootNode.put("af_events_api", "true");
        objRootNode.put("app_version_name", clientBundleVersion);
		
		return objRootNode.toString();
	}
    public static String genAppsflyerPurchaseBodyIos(String clientBundleVersion
                                                    , String appsflyerId
                                                    , String idfa
                                                    , String localPrice
                                                    , String localCurrency
                                                    , String skuId
                                                    , String deviceIp
                                                    , String orderId
                                                    , String sessionId
                                                    , String card
                                                    , String elixir
                                                    , String gem
                                                    , String land
                                                    , String eventId) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		
		JsonObject jsonValue = JsonObject.create()
                                .put("af_client_version", clientBundleVersion)
                                .put("Session ID", sessionId)
                                .put("af_sku_id", skuId)
                                .put("Card", card)
                                .put("Elixir", elixir)
                                .put("Gem", gem)
                                .put("Land", land)
                                .put("af_order_id", orderId)
                                .put("af_revenue", localPrice)
                                .put("af_currency", localCurrency)
                                .put("EventID", eventId);
        String evtValueStr = jsonValue.toString();
		ObjectNode objRootNode = mapper.createObjectNode();
		objRootNode.put("appsflyer_id", appsflyerId);
        objRootNode.put("idfa", idfa);
        objRootNode.put("bundle_id", Const.PROJX_BUNDLE_ID);
        objRootNode.put("eventName", "af_purchase");
        objRootNode.put("eventValue", evtValueStr);
        objRootNode.put("eventCurrency", localCurrency);
        objRootNode.put("ip", deviceIp);
        objRootNode.put("eventTime", Misc.getCurrentUtcTime());
        objRootNode.put("af_events_api", "true");
        objRootNode.put("app_version_name", clientBundleVersion);
		
		return objRootNode.toString();
	}
    
    // Utils
    public static String convertPlatform4Log(String platform) {
        switch(platform)
        {
            case "ANDROID":
            case "Android":
            case "android":
                return "Google";
            case "IOS":
            case "Ios":
            case "ios":
                return "Apple";
        }
        return platform;
    }
}