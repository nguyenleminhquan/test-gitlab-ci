package projectx.documents;

import java.util.Base64;

import com.couchbase.client.core.util.*;
import com.couchbase.client.java.json.JsonObject;
import com.google.protobuf.ByteString;

import firebat.db.memcached.Document;

public class BackupDataDocument extends Document {
    public static final String KEY_PREFIX = "USER_BACKUP_DATA_";

    public String userId;
    public ByteString userData;

    public BackupDataDocument(String userId, ByteString userData) {
        this.userId = userId;
        this.userData = userData;
    }

    public BackupDataDocument(JsonObject jsonObject) {
        userId = jsonObject.getString("userId");
        userData = ByteString.copyFrom(Base64.getDecoder().decode(jsonObject.getString("userData")));
    }

    @Override
    public JsonObject toJson() {
        return JsonObject.create()
                .put("userId", userId)
                .put("userData", Base64.getEncoder().encodeToString(userData.toByteArray()));
    }
}
