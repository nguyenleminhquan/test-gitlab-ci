package projectx.database;

public class KeyName {
    public 	final static char SEPERATOR = '_';
    private final static int INIT_CAPACITY = 32;
    // suffixs
    private final static String SUFFIX_USER = "user";
    
    public static String user (String userId) {
        return new StringBuilder(INIT_CAPACITY)
                .append(userId)
                .append(SEPERATOR)
                .append(SUFFIX_USER)
                .toString();
    }
}
