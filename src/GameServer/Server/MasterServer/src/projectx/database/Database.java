package projectx.database;

import firebat.db.memcached.AbstractDbKeyValue;
import firebat.db.memcached.BucketManager;

public class Database {
    public static AbstractDbKeyValue BUCKET_CACHE;
    public static AbstractDbKeyValue BUCKET_BASE;

    public static void init() {
        BUCKET_BASE = BucketManager.get("lgrbase");
        BUCKET_CACHE = BucketManager.get("lgrcache");
    }

}
