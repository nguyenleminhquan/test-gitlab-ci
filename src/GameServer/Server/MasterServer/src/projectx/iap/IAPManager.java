package projectx.iap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import firebat.log.Log;

public class IAPManager {
    private static List<IAPTestUserInfo> testUserInfo;
    private static final String testIdFilePath = "./data/iapTestUserInfo.txt";

    public static void init() {
        getTestUserInfo();
        getCheatUserInfo();
    }

    public static boolean isTestUser(String userId) {
        for (IAPTestUserInfo iapTestUserInfo : testUserInfo) {
            if (userId.equals(iapTestUserInfo.userId))
                return true;
        }
        return false;
    }

    public static void getTestUserInfo() {
        System.out.println("--- Get IAP Test User Info");
        testUserInfo = new ArrayList<IAPTestUserInfo>();
        List<String> testRawDatas = getUserRawDataFromFile(testIdFilePath);
        for (String rawData : testRawDatas) {
            IAPTestUserInfo info = new IAPTestUserInfo(rawData);
            testUserInfo.add(info);
            System.out.println("Google id: " + info.userId + ", gmail: " + info.mail + ".");
        }
        System.out.println("--- IAP Test User Count: " + testUserInfo.size());
    }

    public static String addTestUser(String userId, String mail) {
        if (isTestUser(userId)) {
            return "already exist";
        } else {
            IAPTestUserInfo info = new IAPTestUserInfo(userId, mail);
            testUserInfo.add(info);
            System.out.println("--- Add Test User: " + userId);
            return "success";
        }
    }

    public static void getCheatUserInfo() {
        // TODO: get cheater from file
    }

    private static List<String> getUserRawDataFromFile(String filePath) {
        List<String> ids = new ArrayList<String>();

        if (new File(filePath).exists()) {
            try {
                FileReader reader = new FileReader(filePath);
                BufferedReader bufferedReader = new BufferedReader(reader);
                String id = null;
                do {
                    id = bufferedReader.readLine();
                    if (id != null) {
                        ids.add(id);
                    }
                } while (id != null);
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return ids;
    }
}