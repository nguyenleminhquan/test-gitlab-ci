package projectx.iap;

import firebat.log.Log;

public class IAPTestUserInfo extends IAPUserInfoGeneral {
    public IAPTestUserInfo(String userId, String mail) {
        this.userId = userId;
        this.mail = mail;
    }

    public IAPTestUserInfo(String rawData) {
        try {
            String[] split = rawData.split("[|]");
            userId = split[0];
            mail = split[1];
        } catch (Exception e) {
            Log.debug("Parse user info error: " + e.getMessage());
            userId = "unknown";
            mail = "unknown";
        }
    }
}
