package projectx.io;

import projectx.controllers.*;
import projectx.views.*;
import projectx.iap.IAPManager;
import projectx.utils.ProjXHelper;
import firebat.db.memcached.AbstractDbKeyValue;
import firebat.db.memcached.DocumentTest;
import firebat.log.Log;
import firebat.utils.*;

import com.google.gson.JsonObject;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.EventLoopGroup;
import io.netty.handler.codec.http.*;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.CharsetUtil;
import io.netty.util.ReferenceCountUtil;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import static io.netty.handler.codec.http.HttpHeaders.Names.*;
import static io.netty.handler.codec.http.HttpHeaders.Values.NO_CACHE;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

/**
 * Updated by hungbt (author @thuanvt) on 01/04/2023.
 */
 
public class GameServerHandler extends ChannelInboundHandlerAdapter {
    private final static AtomicInteger numConnection = new AtomicInteger();

    private ChannelHandlerContext ctx;
	private UserController  userController = null;
	private UserView  userView = null;
    

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        numConnection.incrementAndGet();
        //Log.debug("[GAME ACTIVE]", numConnection.get());
        this.ctx = ctx;
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        numConnection.decrementAndGet();
        //Log.debug("[GAME INACTIVE]", numConnection.get());
    }

    static String[] accounts = {
            "toanpdm", "Aa123456"
    };

    public boolean authen(Map<String, List<String>> params) {
        if (!params.isEmpty()) {
            try {
                String admin = getValue(params, "admin");
                String password = getValue(params, "password");
                for (int i = 0; i < accounts.length; i += 2) {
                    if (accounts[i].equals(admin) && accounts[i + 1].equals(password))
                        return true;
                }
                return false;
            } catch (Exception e) {
                return false;
            }
        } else {
            return false;
        }
    }

    public String getValue(Map<String, List<String>> params, String key) {
        if (!params.containsKey(key))
            return null;
        List<String> value = params.get(key);
        if (value == null || value.size() == 0)
            return null;
        return value.get(0);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		//Log.debug("Channel read!");
        // System.out.println("Channel Read!");
        try{
            if (msg instanceof HttpRequest) {
                HttpRequest request = (HttpRequest) msg;
                QueryStringDecoder queryStringDecoder = new QueryStringDecoder(request.getUri());
                Map<String, List<String>> params = queryStringDecoder.parameters();
                String path = queryStringDecoder.path();
                boolean isAuthen = authen(params);
                switch (path) {
                    case "/alive":
                        writeTextAndClose(ctx, "yes");
                        break;
                    case "/status":
                        getStatus(ctx);
                        break;
                    case "/database":
                        getConnectDBStatus(ctx);
                        break;
                    case "/addTestUser":
                        if (isAuthen) {
                            try {
                                String userId = getValue(params, "userId");
                                String result = IAPManager.addTestUser(userId, "unknown");
                                writeTextAndClose(ctx, "Add test user: " + result);
                            } catch (Exception e) {
                                writeTextAndClose(ctx, "Add test user error: " + e.toString());
                            }
                        } else {
                            writeTextAndClose(ctx, "Wrong admin or password");
                        }
                        break;
                }
            }

            if (msg instanceof HttpContent) {
                HttpContent httpContent = (HttpContent) msg;
                ByteBuf content = httpContent.content();
                if (content.isReadable()) {
                    int cmdId = content.readInt();
                    int crc = content.readInt();
                    if (crc == 0) {
                        if (userController == null) {
                            if(userView == null) {
                                userView = new UserView(this);
                            }
                            userController = new UserController(userView);
                        }
                        userController.handle(cmdId, null);
                    }
                    else {
                        byte[] bytes = new byte[content.readableBytes()];
                        content.readBytes(bytes);
                        int fuCRC = Hash.intHash(bytes);
                        // System.out.println("Compute crc");
                        if (bytes.length > 0 && (fuCRC == crc)) {
                            if (userController == null) {
                                if(userView == null) {
                                    // System.out.println("Create View");
                                    userView = new UserView(this);
                                }
                                // System.out.println("Create Controller");
                                userController = new UserController(userView);
                            }
                            Log.debug("Handle command " + cmdId);
                            userController.handle(cmdId, bytes);
                        }
                        else {
                            close();
                        }
                    }
                }
                else {
                    close();
                }
            }
        } finally {
            ReferenceCountUtil.release(msg);
        }
        
    }
	
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
        if (cause.toString().contains("java.io.IOException: Connection reset by peer")) {
            return;
        }
		Log.exception(cause);
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            Log.debug("[IDLE]");
            ctx.close();
        }
    }

    public void close() {
        ctx.close();
    }

    public static int getNumConnection() {
        return numConnection.get();
    }
	
	public String getClientIp()	{
		InetSocketAddress socketAddress = (InetSocketAddress) ctx.channel().remoteAddress();
		return socketAddress.getAddress().getHostAddress();
	}
	
	public EventLoopGroup getLoopGroup() {
		if(ctx==null)
			return null;
		return ctx.channel().eventLoop();
	}
	
	public static void getStatus(ChannelHandlerContext ctx) {
        JsonObject json = new JsonObject();
        json.addProperty("connection", getNumConnection());
        json.addProperty("available processors", Monitor.availableProcessors());

        json.addProperty("Process cpu used percent", Monitor.getProcessCpuUsedPercent() + " %");
        json.addProperty("System cpu used percent", Monitor.getSystemCpuUsedPercent() + " %");

        json.addProperty("Physical memory total", Monitor.getPhysicalMemTotal() + " Mb");
        json.addProperty("Physical memory used", Monitor.getPhysicalMemUsed() + " Mb");
        json.addProperty("Physical used percent", Monitor.getPhysicalMemUsedPercent() + " %");

        json.addProperty("Heap memory total", Monitor.getHeapMemoryTotal() + " Mb");
        json.addProperty("Heap memory used", Monitor.getHeapMemoryUsed() + " Mb");
        json.addProperty("Heap used percent", Monitor.getMemoryUsedPercent() + " %");

        writeTextAndClose(ctx, Json.gsonPretty.toJson(json));
    }

    public static void getConnectDBStatus(ChannelHandlerContext ctx) {
        JsonObject json = new JsonObject();
        try {
            DocumentTest connectStatus = ProjXHelper.getDocumentFromDB(DocumentTest.class, AbstractDbKeyValue.TEST_KEY);
            json.addProperty("Database Name", connectStatus.dbName);
            json.addProperty("Num retry", connectStatus.numRetry);
            json.addProperty("Time create", connectStatus.timeCreated);
            writeTextAndClose(ctx, Json.gsonPretty.toJson(json));
        } catch (Exception e) {
            writeTextAndClose(ctx, "Connect database error: " + e.getMessage());
        }

    }
	
	private static void writeTextAndClose(ChannelHandlerContext ctx, CharSequence cs) {
        writeHttpResponseAndClose(ctx, Unpooled.copiedBuffer(cs, CharsetUtil.UTF_8), "text/plain; charset=UTF-8");
    }

    private static void writeHttpResponseAndClose(ChannelHandlerContext ctx, ByteBuf buf, String contentType) {
        DefaultFullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, OK, buf);
        HttpHeaders headers = response.headers();
        headers.set(CONTENT_TYPE, contentType);
        headers.set(CACHE_CONTROL, NO_CACHE);
        headers.set(CONTENT_LENGTH, buf.readableBytes());

        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }
	
    public void writeTextAndClose(CharSequence cs) {
		Log.debug(cs.toString());
        writeHttpResponseAndClose(Unpooled.copiedBuffer(cs, CharsetUtil.UTF_8), "text/plain; charset=UTF-8");
    }

    public void writeHtmlAndClose(CharSequence cs) {
        writeHttpResponseAndClose(Unpooled.copiedBuffer(cs, CharsetUtil.UTF_8), "text/html; charset=UTF-8");
    }

    public void writeBinAndClose(byte[] bin) {
        writeHttpResponseAndClose(Unpooled.wrappedBuffer(bin), "application/octet-stream");
    }

    public void writeBinAndClose(ByteBuf buf) {
        writeHttpResponseAndClose(buf, "application/octet-stream");
    }

    public void writeHttpResponseAndClose(ByteBuf buf, String contentType) {
        DefaultFullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, OK, buf);
        HttpHeaders headers = response.headers();
        headers.set(CONTENT_TYPE, contentType);
        headers.set(CACHE_CONTROL, NO_CACHE);
        headers.set(CONTENT_LENGTH, buf.readableBytes());

        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }
}
