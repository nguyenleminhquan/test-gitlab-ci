package projectx.controllers;

import projectx.documents.BackupDataDocument;
import projectx.protocols.*;
import projectx.utils.*;
import com.google.protobuf.*;

import firebat.log.Log;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
/**
 * Created by hungbt
 */
public class CommandLoadData extends BaseCommandProcess {
    
    public CommandLoadData(byte[] data) {
        super(data);
    }

    @Override
    public byte[] process() throws Exception {
        // get params
        LoadDataRequest lReq = LoadDataRequest.parseFrom(rawData);
        String userId = lReq.getUserId();
        // process...
        int retCode = CmdResult.FAIL.getNumber();
        ByteString userData = ByteString.copyFrom("None".getBytes());
        
        if(userId != "")
        {
            BackupDataDocument document = ProjXHelper.getDocumentFromDB(BackupDataDocument.class,
                    BackupDataDocument.KEY_PREFIX + userId);
            if (document != null) {
                retCode = CmdResult.SUCCESS.getNumber();
                userData = document.userData;
            }
        }
        
        // resp
        LoadDataResponse.Builder lRespBuilder = LoadDataResponse.newBuilder()
                                                            .setRetCode(retCode)
                                                            .setUserData(userData);
        return lRespBuilder.build().toByteArray();
    }
}