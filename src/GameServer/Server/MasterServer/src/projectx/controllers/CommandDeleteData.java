package projectx.controllers;

import projectx.documents.BackupDataDocument;
import projectx.protocols.*;
import projectx.utils.*;
import com.google.protobuf.*;

public class CommandDeleteData extends BaseCommandProcess {

    public CommandDeleteData(byte[] data) {
        super(data);
    }

    @Override
    public byte[] process() throws Exception {
        // get params
        DeleteDataRequest request = DeleteDataRequest.parseFrom(rawData);
        String userId = request.getUserId();
        // process...
        int retCode = CmdResult.FAIL.getNumber();
        String retMessage = "";

        if (userId != "") {
            boolean success = ProjXHelper.deleteDocumentInDB(BackupDataDocument.KEY_PREFIX + userId);
            if (success) {
                retCode = CmdResult.SUCCESS.getNumber();
                retMessage = "Success";
            } else {
                retMessage = "DBErrors";
            }
        } else {
            retMessage = "InvalidParams";
        }

        // resp
        DeleteDataResponse.Builder lRespBuilder = DeleteDataResponse.newBuilder()
                .setRetCode(retCode)
                .setMessage(retMessage);
        return lRespBuilder.build().toByteArray();
    }
}