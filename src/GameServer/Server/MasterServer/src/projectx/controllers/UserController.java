package projectx.controllers;

import projectx.views.*;
import projectx.protocols.*;

/**
 * Updated by hungbt (author @thuanvt) on 01/04/2023.
 */
 
public class UserController {
    private UserView userView;
	private int cmdId;

	public UserController(UserView uView) {
		userView = uView;
	}
    
    public void handle(int commandId, byte[] data) throws Exception {
		cmdId = commandId;
		handleCommand(data);
	}

    public void handleCommand (byte[] data) throws Exception {
        byte [] respData = null;
        boolean requestCloseChannel = true;
        // Convert int to enum
        CmdId commandId = CmdId.forNumber(cmdId);
        BaseCommandProcess cmdProcess = null;
        switch (commandId) {
            case LOGIN: {
                cmdProcess = new CommandLogin(data);
                break;
            }
            case LOAD_DATA: {
                cmdProcess = new CommandLoadData(data);
                break;
            }
            case BACKUP_DATA: {
                cmdProcess = new CommandBackupData(data);
                break;
            }
            case VERIFY_IAP: {
                cmdProcess = new CommandVerifyIAP(data, userView.getClientIp());
                break;
            }
            case DELETE_DATA: {
                cmdProcess = new CommandDeleteData(data);
                break;
            }
        }
        /// Process
        if(cmdProcess != null) {
            respData = cmdProcess.process();
            /// Response for client
            if(respData != null) {
                userView.response(respData);
            }
            else {
                if(requestCloseChannel) {
                    userView.close();
                }
            }
        } else {
            userView.close();
        }
    }
}