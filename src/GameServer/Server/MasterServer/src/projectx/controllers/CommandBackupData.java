package projectx.controllers;

import projectx.documents.BackupDataDocument;
import projectx.protocols.*;
import projectx.utils.*;
import com.google.protobuf.*;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
/**
 * Created by hungbt
 */
public class CommandBackupData extends BaseCommandProcess {
    
    public CommandBackupData(byte[] data) {
        super(data);
    }

    @Override
    public byte[] process() throws Exception {
        // get params
        BackupDataRequest lReq = BackupDataRequest.parseFrom(rawData);
        String userId = lReq.getUserId();
        ByteString userData = lReq.getUserData();
        int retCode = CmdResult.FAIL.getNumber();
        String retMessage = "";
        if(userId != "" && userData != null) {
            BackupDataDocument document = new BackupDataDocument(userId, userData);
            if (ProjXHelper.replaceDocumentInDB(BackupDataDocument.KEY_PREFIX + userId, document)) {
                retCode = CmdResult.SUCCESS.getNumber();
                retMessage = "Success";
            } else {
                retMessage = "DBErrors";
            }
        } else {
            retMessage = "InvalidParams";
        }
        // resp
        BackupDataResponse.Builder lRespBuilder = BackupDataResponse.newBuilder()
                                                            .setRetCode(retCode)
                                                            .setMessage(retMessage);
        return lRespBuilder.build().toByteArray();
    }
}