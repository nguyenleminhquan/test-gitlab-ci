package projectx.controllers;

import projectx.Main;
import projectx.iap.IAPManager;
import projectx.protocols.*;
import projectx.utils.*;
import projectx.models.*;
import firebat.utils.*;

import com.couchbase.client.java.json.JsonObject;
import com.google.protobuf.*;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;


/**
 * Created by hungbt
 */
public class CommandVerifyIAP extends BaseCommandProcess {

    private String deviceIp;

    public CommandVerifyIAP(byte[] data, String deviceIp) {
        super(data);
        this.deviceIp = deviceIp;
    }

    @Override
    public byte[] process() throws Exception {
        // get params
        VerifyIAPRequest lReq = VerifyIAPRequest.parseFrom(rawData);
        String platform = lReq.getPlatform();
        String buildVersion = lReq.getBuildVersion();
        String userId = lReq.getUserId();
        String skuId = lReq.getSkuIAP();
        String tranCurrency = lReq.getCurrency();
        String tranPrice = lReq.getLocalPrice();
        String receipt = lReq.getReceipt();
        String appsflyerId = lReq.getAppsflyerId();
        String sessionId = lReq.getSessionId();
        String card = lReq.getCard();
        String elixir = lReq.getElixir();
        String gem = lReq.getGem();
        String land = lReq.getLand();
        String eventId = lReq.getEventId();
        // process...
        int retCode = CmdResult.FAIL.getNumber();
        boolean isSuccess = false;
        boolean isTestUser = IAPManager.isTestUser(userId);
        if(userId != "" && platform != "" && receipt != "" && skuId != "")
        {
            boolean isLiveEnv = false;
            boolean needLog = false;
            boolean isVerified = false;
            switch(platform)
            {
                case "ANDROID":
                    ReceiptFormat fields =  Json.fromJson(receipt, ReceiptFormat.class);
                    if(fields.Payload != null && fields.Payload != "")
                    {
                        DetailPayload detail =  Json.fromJson(fields.Payload, DetailPayload.class);
                        if (detail != null && detail.signature != null && detail.json != null) {
                            isVerified = ProjXHelper.verifyReceiptAndroid(detail.signature, detail.json);
                            ReceiptInfo rifo = Json.fromJson(detail.json, ReceiptInfo.class);
                            String transactionId = "empty";
                            if(rifo != null)
                                transactionId = rifo.orderId;

                            needLog = Main.environment != Environment.DEV
                                    && isVerified
                                    && appsflyerId != null
                                    && tranCurrency != null
                                    && tranCurrency != ""
                                    && tranPrice != null
                                    && tranPrice != ""
                                    && !isTestUser;

                            if(needLog)
                            {
                                ProjXHelper.sendLogPurchaseToAppsflyer(true
                                                                        , buildVersion
                                                                        , appsflyerId
                                                                        , tranPrice
                                                                        , tranCurrency
                                                                        , skuId
                                                                        , deviceIp
                                                                        , transactionId
                                                                        , sessionId
                                                                        , card
                                                                        , elixir
                                                                        , gem
                                                                        , land
                                                                        , eventId);
                                isLiveEnv = true;
                            }
                        }
                    }
                    break;
                case "IOS":
                    ObjectMapper mapperIOS = new ObjectMapper();
                    JsonNode payload = mapperIOS.readTree(receipt).findValue("Payload");
                    String resultReceipt = ProjXHelper.verifyReceiptIos(payload.getTextValue(), Main.environment == Environment.DEV);
                    if(resultReceipt!=null) {
                        ObjectMapper mapper = new ObjectMapper();
                        JsonNode resultObject = mapper.readTree(resultReceipt);
                        JsonNode productidN = resultObject.findValue("product_id");
                        JsonNode tranidN = resultObject.findValue("transaction_id");
                        JsonNode environment = resultObject.findValue("environment");
                        if(productidN != null && tranidN != null) {
                            isVerified = true;
                            needLog = Main.environment != Environment.DEV
                                    && !environment.getTextValue().equals("Sandbox")
                                    && !isTestUser;

                            if(needLog) {
                                ProjXHelper.sendLogPurchaseToAppsflyer(false
                                                                        , buildVersion
                                                                        , appsflyerId
                                                                        , tranPrice
                                                                        , tranCurrency
                                                                        , skuId
                                                                        , deviceIp
                                                                        , tranidN.getTextValue()
                                                                        , sessionId
                                                                        , card
                                                                        , elixir
                                                                        , gem
                                                                        , land
                                                                        , eventId);
                                isLiveEnv = true;
                            }
                        }
                    }
                    break;
            }
            if(isVerified) {
                retCode = CmdResult.SUCCESS.getNumber();
                isSuccess = true;
                if(isLiveEnv && needLog) {
                    ProjXHelper.writeLogIFRS(userId
                                                , platform
                                                , skuId
                                                , tranPrice
                                                , tranCurrency);
                }
                else if(!isLiveEnv){
                    JsonObject jsonDebug = JsonObject.create()
                                            .put("platform",platform)
                                            .put("buildVersion",buildVersion)
                                            .put("userId",userId)
                                            .put("deviceIp", deviceIp)
                                            .put("skuId",skuId)
                                            .put("tranCurrency",tranCurrency)
                                            .put("tranPrice",tranPrice)
                                            .put("receipt",receipt)
                                            .put("appsflyerId",appsflyerId)
                                            .put("sessionId",sessionId)
                                            .put("card",card)
                                            .put("elixir",elixir)
                                            .put("gem",gem)
                                            .put("land",land)
                                            .put("eventId",eventId);
                    System.out.println("af_purchase: " + jsonDebug.toString());                                            
                }

                ///////////////////////////////////////////////////////////////////////////////////
            }
            else {
                retCode = CmdResult.SUCCESS.getNumber();
                isSuccess = false;
            }
        }
        
        // resp
        VerifyIAPResponse.Builder lRespBuilder = VerifyIAPResponse.newBuilder()
                                                            .setRetCode(retCode)
                                                            .setIsSuccess(isSuccess);
        return lRespBuilder.build().toByteArray();
    }
}