package projectx.models;
/**
 * Created by hungbt
 */
public class ReceiptInfo
{
	public String orderId;
	public String packageName;
	public String productId;
	public long purchaseTime;
	public int purchaseState;
	public String developerPayload;
	public String purchaseToken;
}