package firebat.io.socket;

import firebat.log.Log;
import firebat.utils.Address;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.util.concurrent.TimeUnit;

/**
 * Created by thuanvt on 10/14/2014.
 */
public class SocketServer {
    private String host;
    private int port;
    private ServerBootstrap bootstrap;
    private NioEventLoopGroup bossGroup, workerGroup;

    public SocketServer() {
        bootstrap = new ServerBootstrap()
                .channel(NioServerSocketChannel.class)
                .childOption(ChannelOption.WRITE_BUFFER_HIGH_WATER_MARK, 32 * 1024)
                .childOption(ChannelOption.WRITE_BUFFER_LOW_WATER_MARK, 8 * 1024)
                .childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childOption(ChannelOption.TCP_NODELAY, true);
    }

    public ServerBootstrap getBootstrap() {
        return bootstrap;
    }

    public synchronized boolean start(String host, int port, ChannelInitializer<SocketChannel> initializer) throws Exception {
        if (bossGroup != null || workerGroup != null)
            return false;

        this.host = host;
        this.port = port;
        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();

        bootstrap.group(bossGroup, workerGroup)
                .childHandler(initializer)
                .bind(Address.getInetSocketAddress(host, port))
                .sync();

        Log.info("Start socket server", host, port);
        return true;
    }


    public synchronized boolean stop() {
        return stop(100, 15000, TimeUnit.MILLISECONDS);
    }

    public synchronized boolean stop(int quietPeriod, int timeout, TimeUnit unit) {
        if (bossGroup == null || workerGroup == null)
            return false;

        Log.info("Stop socket server", host, port);
        bossGroup.shutdownGracefully(quietPeriod, timeout, unit);
        bossGroup = null;

        workerGroup.shutdownGracefully(quietPeriod, timeout, unit);
        workerGroup = null;
        return true;
    }
}
