package firebat.io.socket;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * Created by thuanvt on 10/14/2014.
 */
public class SocketServerDecoder extends ByteToMessageDecoder {
    final static int HEADER_LEN = 4;
    final static int MAX_CLIENT_PACKAGE_SIZE = 65000;

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        if (in.readableBytes() <= HEADER_LEN) //4 byte len, x byte data
            return;
        in.markReaderIndex();
        int bodyLen = in.readInt();
        if (bodyLen <= 0 || bodyLen > MAX_CLIENT_PACKAGE_SIZE) {
            ctx.close();
            return;
        }
        if (bodyLen < in.readableBytes()) {
            in.resetReaderIndex();
            return;
        }
        out.add(in.readBytes(bodyLen));
    }
}
