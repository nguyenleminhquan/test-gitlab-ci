package firebat.utils;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class Hash
{
    public static byte[] toByteArray (Charset charset, Object... ao)
    {
        StringBuilder sb = new StringBuilder(256);
        for (Object o : ao)
        {
            sb.append(o);
        }
        if (charset == null)
            return sb.toString().getBytes(StandardCharsets.UTF_8);
        else
            return sb.toString().getBytes(charset);
    }
    
    //algorithm: MD5, SHA-1, SHA-256    
    public static String hash (String algorithm, Object... ao)
    {
        return hash(algorithm, toByteArray(null, ao));
    }
    
	public static String hash (byte[] data)
	{
		return hash("MD5", data);
	}
	
    public static String hash (String algorithm, byte[] data)
    {
        try
        {   
            MessageDigest digester = MessageDigest.getInstance(algorithm);
            byte[] bytes = digester.digest(data);
            
            StringBuilder result = new StringBuilder(64);
            for (byte b : bytes)
            {
                int val = 0xff & b;
                if (val <= 0x0f)
                {
                    result.append('0');
                }
                result.append(Integer.toHexString(val));
            }

            return result.toString();
        }
        catch (Exception e)
        {
//            e.printStackTrace();
        }
        return null;
    }
    
    public static int murmurHash3_x86_32 (int seed, Object... ao)
    {
        return murmurHash3_x86_32(seed, toByteArray(null, ao));
    }
    
    public static int murmurHash3_x86_32 (int seed, byte[] data)
    {
        int len = data.length;

        final int c1 = 0xcc9e2d51;
        final int c2 = 0x1b873593;

        int h1 = seed;
        int roundedEnd = (len & 0xfffffffc);  // round down to 4 byte block

        for (int i = 0; i < roundedEnd; i += 4)
        {
            // little endian load order
            int k1 = (data[i] & 0xff) | ((data[i + 1] & 0xff) << 8) | ((data[i + 2] & 0xff) << 16) | (data[i + 3] << 24);
            k1 *= c1;
            k1 = (k1 << 15) | (k1 >>> 17);  // ROTL32(k1,15);
            k1 *= c2;

            h1 ^= k1;
            h1 = (h1 << 13) | (h1 >>> 19);  // ROTL32(h1,13);
            h1 = h1 * 5 + 0xe6546b64;
        }

        // tail
        int k1 = 0;

        switch (len & 0x03)
        {
            case 3:
                k1 = (data[roundedEnd + 2] & 0xff) << 16;
            // fallthrough
            case 2:
                k1 |= (data[roundedEnd + 1] & 0xff) << 8;
            // fallthrough
            case 1:
                k1 |= (data[roundedEnd] & 0xff);
                k1 *= c1;
                k1 = (k1 << 15) | (k1 >>> 17);  // ROTL32(k1,15);
                k1 *= c2;
                h1 ^= k1;
        }

        // finalization
        h1 ^= len;

        // fmix(h1);
        h1 ^= h1 >>> 16;
        h1 *= 0x85ebca6b;
        h1 ^= h1 >>> 13;
        h1 *= 0xc2b2ae35;
        h1 ^= h1 >>> 16;

        return h1;
    }
	
	
	private static final int HASH_KEY_COMMAND = 568605691;
	
	public static int intHash (byte[] bytes)
	{			
		int h = bytes.length;
		int rem = h & 3;
		int j = 0;
		int i = (int)(h & 0xfffffffc);		
		h += HASH_KEY_COMMAND;			
		while (j < i)
		{
			h += (bytes[j++] & 0xff) | ((bytes[j++] & 0xff) << 8);			
			h = (h << 16) ^ ((((bytes[j++] & 0xff) | ((bytes[j++] & 0xff) << 8)) << 11) ^ h);
			h += h >> 11;
		}		
		switch (rem)
		{
			case 1:			
				h += (bytes[j++] & 0xff);
				h ^= h << 10;
				h += h >> 1;	
				break;
			case 2:			
				h += (bytes[j++] & 0xff) | ((bytes[j++] & 0xff) << 8);
				h ^= h << 11;
				h += h >> 17;				
				break;
			case 3:			
				h += (bytes[j++] & 0xff) | ((bytes[j++] & 0xff) << 8);
				h ^= h << 16;
				h ^= (bytes[j++] & 0xff) << 18;
				h += h >> 11;				
				break;
		}
		h ^= h << 3;
		h += h >> 5;
		h ^= h << 4;
		h += h >> 17;
		h ^= h << 25;
		h += h >> 6;
		return h;
	}
}
