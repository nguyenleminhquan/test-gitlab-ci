package firebat.utils;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.DataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Writer;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.Random;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.Locale;
import java.util.zip.CRC32;
import java.util.zip.Deflater;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

public class Misc
{
    private static final Random s_pRandom = new Random();
    /**
     *
     */
    public static int divCeil (int num, int div)
    {
        if (num % div == 0)
        {
            return num / div;
        }
        else
        {
            return (num / div) + 1;
        }
    }

    /**
     *
     */
    public static byte[] readBytes(DataInputStream dis)
    {
        try
        {
            int len = dis.readShort();
            
            if (len > 0)
            {
                byte[] bytes = new byte[len];
                
                dis.read(bytes);
                
                return bytes;
            }
        }
        catch (Exception ex)
        {
            
        }
        
        return null;
    }

    /**
     *
     */
    static public String readString(DataInputStream dis)
    {
        try
        {
            int len = dis.readByte()&0xFF;
            
            if (len > 0)
            {
                byte[] bstr = new byte[len];
                
                dis.read(bstr);
                
                String str;
                try
                {
                    str = new String(bstr, "UTF-8");
                }
                catch(Exception e)
                {
                    str = new String(bstr);
                }
                
                return str;
            }
        }
        catch (Exception ex)
        {
            
        }
        
        return "";
    }
    
    /**
     *
     */
    static public void writeBytes(DataOutputStream dos, byte[] bytes)
    {
        try
        {
            if (bytes == null)
            {
                dos.writeShort(0);
                return;
            }
            
            int len = bytes.length;
            dos.writeShort(len);
            
            dos.write(bytes, 0, len);
        }
        catch (Exception ex)
        {
        }
    }
    
    /**
     *
     */
    static public void writeString(DataOutputStream dos, String value)
    {
        try
        {
            if (value == null || value.length() == 0)
            {
                dos.write(0);
                return;
            }
            byte[] bytes = null;
            try
            {
                bytes = value.getBytes("UTF-8");
            }
            catch(Exception e)
            {
                bytes = value.getBytes();
            }
            
            int len = bytes.length;
            dos.write(len);
            dos.write(bytes, 0, len);
        }
        catch (Exception ex)
        {
            
        }
    }
    
    /**
     *
     */
    static public byte[] getBytes(String string)
    {        
        if (string == null)
        {
            return null;
        }
        byte[] strbuf = null;
        try
        {
            strbuf = string.getBytes("UTF-8");
        }
        catch(Exception e)
        {
            strbuf = string.getBytes();
        }
        
        return strbuf;
    }
    
    /**
     *
     */
    static public long getMemUsage()
    {
        Runtime rt = Runtime.getRuntime();
        return rt.totalMemory() - rt.freeMemory();
    }

    /**
     *
     */
    public static String joinString(Object... params) throws Exception
    {
       StringBuilder sb = new StringBuilder();
       for (int i=1; i<params.length; i++)
       {
           sb.append(params[i]);
       }
       return sb.toString();
    }

    /**
     *  Load texts from file
     */
    public static String loadTexts(String strFilePath)
    {
        return loadTexts(strFilePath, null);
    }
    /**
     *  Load texts from file
     */
    public static String loadTexts(String strFilePath, String strEncode)
    {
        try
        {
            DataInputStream dis = new DataInputStream(new FileInputStream(strFilePath));

            byte[] buff = new byte[dis.available()];
            dis.read(buff);
            dis.close();
            dis = null;
            if (buff != null)
            {
                if (strEncode == null)
                {
                    return new String(buff);
                }
                return new String(buff, 0, buff.length, strEncode);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    
    /**
     *  CRC decode from javascript
     */
    static final int[] aCRC32Table =
    {
        0x0, 0x77073096, 0xEE0E612C, 0x990951BA, 0x76DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3, 0xEDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988, 0x9B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91, 0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE, 0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7, 0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC, 0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5, 0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172, 0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B, 0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940, 0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59, 0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116, 0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F, 0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924, 0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D, 0x76DC4190, 0x1DB7106, 0x98D220BC, 0xEFD5102A, 0x71B18589, 0x6B6B51F, 0x9FBFE4A5, 0xE8B8D433, 0x7807C9A2, 0xF00F934, 0x9609A88E, 0xE10E9818, 0x7F6A0DBB, 0x86D3D2D, 0x91646C97, 0xE6635C01, 0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E, 0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457, 0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C, 0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65, 0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2, 0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB, 0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0, 0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9, 0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086, 0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F, 0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4, 0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD, 0xEDB88320, 0x9ABFB3B6, 0x3B6E20C, 0x74B1D29A, 0xEAD54739, 0x9DD277AF, 0x4DB2615, 0x73DC1683, 0xE3630B12, 0x94643B84, 0xD6D6A3E, 0x7A6A5AA8, 0xE40ECF0B, 0x9309FF9D, 0xA00AE27, 0x7D079EB1, 0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE, 0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7, 0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC, 0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5, 0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252, 0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B, 0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60, 0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79, 0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236, 0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F, 0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04, 0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D, 0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x26D930A, 0x9C0906A9, 0xEB0E363F, 0x72076785, 0x5005713, 0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0xCB61B38, 0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0xBDBDF21, 0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E, 0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777, 0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C, 0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45, 0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2, 0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB, 0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0, 0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9, 0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6, 0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF, 0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94, 0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
    };
    public static int crc32(String sMessage)
    {
        int iCRC = 0xFFFFFFFF;
        for (int i=0;i<sMessage.length();i++)
        {
            int bytC = sMessage.charAt(i);
            int bytT = (iCRC & 0xFF) ^ bytC;
            int lngA = iCRC >> 8;
            iCRC = lngA ^ aCRC32Table[bytT];
        }
        return iCRC ^ 0xFFFFFFFF;
    }

    /**
     *
     */
    public static int crc32(byte[] bytes)
    {
        CRC32 crc = new CRC32();
        crc.update(bytes);
        return (int)crc.getValue();
    }

    

    /**
     *  Compress input data and return a new one.
     */
    public static int random(int max)
    {
        if (max <= 0)
        {
            return 0;
        }
        return (Math.abs((s_pRandom).nextInt()) % max);
    }

    /**
     *  Compress input data and return a new one.
     */
    public static int random(int max, int min)
    {
        if (max <= 0 || max < min)
        {
            return 0;
        }
        return (Math.abs((s_pRandom).nextInt()) % (max - min)) + min;
    }

    /**
     *  Divide a value and always be round up
     */
    public static int divideRoundUp(int val1, int val2)
    {
        return (int)(Math.ceil(val1/(double)val2*1.0f));
    }

    /**
     *  Check if having new day
     *  @params:
     *      - Seconds of last time
     *      - Seconds of current time
     */
    public static int dayDiff(long last, long curr)
    {
        int d1 = (int)Math.floor(last/DAY_TO_SECONDS());
        int d2 = (int)Math.floor(curr/DAY_TO_SECONDS());
        return Math.abs(d1-d2);
    }

    /**
     *  Check if having new day
     *  @params:
     *      - Milliseconds of last time
     */
    public static int dayDiff(long last)
    {
        Calendar pTempLastDate = Calendar.getInstance();
        Calendar pLastDate = Calendar.getInstance();
        Calendar pCurDate = Calendar.getInstance();

        pTempLastDate.setTimeInMillis(last);
        pLastDate.set(pTempLastDate.get(Calendar.YEAR), pTempLastDate.get(Calendar.MONTH), pTempLastDate.get(Calendar.DAY_OF_MONTH));

        return (int)Math.round((pCurDate.getTime().getTime() - pLastDate.getTime().getTime())/(DAY_TO_SECONDS()*1000));
    }

    /**
     *
     */
    public static long getNextDayMilllisecond(long time, int day, boolean isEndDay) throws Exception
    {
        Calendar pCurTime = Calendar.getInstance();
        Calendar pNextTime = Calendar.getInstance();

        pCurTime.setTimeInMillis(time);
        int nHour=0, nMinute=0, nSecond=0;
        if (isEndDay)
        {
            nHour = 23;
            nMinute = nSecond = 59;
        }
        pNextTime.set(pCurTime.get(Calendar.YEAR), pCurTime.get(Calendar.MONTH), pCurTime.get(Calendar.DAY_OF_MONTH) + day, nHour, nMinute, nSecond);

        return pNextTime.getTimeInMillis();
    }

    /**
     *
     */
    public static int[] getDateArray(String value)
    {
        int index = value.indexOf('/');
        if (index > 0)
        {
            int nDay = Integer.parseInt(value.substring(0, index));
            value = value.substring(index + 1);
            index = value.indexOf('/');
            if (0 < index && index < value.length()-1)
            {
                int nMonth = Integer.parseInt(value.substring(0, index));
                int nYear = Integer.parseInt(value.substring(index+1));
                return new int[]{ nDay, nMonth-1, nYear };
            }
        }
        return null;
    }

    /**
     *  Convert month to day count
     */
    public static int monthToDayCount(long last, int month)
    {
        Calendar pTempLastDate = Calendar.getInstance();
        Calendar pLastDate = Calendar.getInstance();
        Calendar pNextDate = Calendar.getInstance();

        // For last time
        pTempLastDate.setTimeInMillis(last);
        pLastDate.set(pTempLastDate.get(Calendar.YEAR), pTempLastDate.get(Calendar.MONTH), pTempLastDate.get(Calendar.DAY_OF_MONTH));

        // For next month(s)
        pNextDate.set(pLastDate.get(Calendar.YEAR), pLastDate.get(Calendar.MONTH)+month, pLastDate.get(Calendar.DAY_OF_MONTH));

        return (int)Math.round((pNextDate.getTime().getTime() - pLastDate.getTime().getTime())/(DAY_TO_SECONDS()*1000));
    }

    
    /**
     *  Compress input data and return a new one.
     */
    public static byte[] compress(byte[] data)
    {
        //return data;
        
        if (data != null)
        {                              
            try
            {                                                              
                ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length);
                Deflater compresser = new Deflater();                                                                                                  
                compresser.setInput(data);
                compresser.finish();
                byte[] buf = new byte[16 * 1024]; // 16K
                int len;
                while (!compresser.finished())
                {                                                                              
                    len = compresser.deflate(buf);                 
                    if (len == 0)
                    {                                                                                              
                        break;
                    }
                    bos.write(buf, 0, len);
                }
                compresser.end();
                
                return bos.toByteArray();                                                            
            }
            catch (Exception e)
            {
            }
        }
                
        return null;
    }

    
    /**
     *  Create new folder
     */
    public static boolean createFolder(String path)
    {
        try
        {
            File file = new File(path);
            if (!file.exists())
            {
                file.mkdir();
            }
            return true;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }

    
    /**
     *
     */
    public static int dateChecking(int day, int month, int year)
    {
        int nResult = 0;
        // Year
        if (year < 2010 || 3000 < year)
        {
            nResult |= (1 << 1);
        }
        // Month
        if (month < 1 || 12 < month)
        {
            nResult |= (1 << 2);
        }
        // Day
        int nDayMax = 30;
        if (month == 2)
        {
            if (year % 4 == 0)
            {
                nDayMax = 29;
            }
            else
            {
                nDayMax = 28;
            }
        }
        else
        {
            if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
            {
                nDayMax = 31;
            }
        }
        if (day < 1 || nDayMax < day)
        {
            nResult |= (1 << 3);
        }

        return nResult;
    }

    
    /**
     *
     */
    public static void delay(int time)
    {
        try
        {
            long nCurrentTime = MILLISECONDS();
            while ((MILLISECONDS() - nCurrentTime) < time)
            {
                Thread.yield();
            }
        }
        catch(Exception ex)
        {}
    }

    /**
     *  Print integer array out to screen
     */
    public static void printIntArray(String strHeader, int[] pData)
    {
        System.out.print(strHeader);
        for (int i=0; i<pData.length; i++)
        {
            System.out.print("" + pData[i] + ", ");
        }
        System.out.println("");
    }

    /**
     *
     */
    public static final int DATE_FORMAT_YYYY_MM_DD_HH_MM_SS     = 0;
    public static final int DATE_FORMAT_YYYY_MM_DD_HH_MM        = 1;
    public static final int DATE_FORMAT_YYYY_MM_DD_NO_BAR       = 2;
    public static final String[] DATE_FORMAT_STRINGS =
    {
        "yyyy/MM/dd - HH:mm:ss",
        "yyyy/MM/dd - HH:mm",
        "yyyyMMdd",
    };

    public static String now()
    {
        return millisecondsToDate(MILLISECONDS(), DATE_FORMAT_YYYY_MM_DD_HH_MM_SS);
    }

    public static String millisecondsToDate(long milliseconds)
    {
        return millisecondsToDate(milliseconds, DATE_FORMAT_YYYY_MM_DD_HH_MM);
    }

    /**
     *
     */
    public static String millisecondsToDate(long milliseconds, int format)
    {
        if (format < 0 || DATE_FORMAT_STRINGS.length <= format)
        {
            return "";
        }
        String strFormat = DATE_FORMAT_STRINGS[format];
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(milliseconds);
        SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
        return sdf.format(date.getTime());
    }

    /**
     *
     */
    public static String secondsToTime(int second)
    {
        int nHour = second/3600;
        second -= (nHour * 3600);
        int nMinute = second/60;
        second -= (nMinute*60);
        return ((nHour > 9 ? "" : "0") + nHour + (nMinute > 9 ? ":" : ":0") + nMinute + (second > 9 ? ":" : ":0") + second);
    }

    /**
     *
     */
    public static final String IntToString(int value, int max)
    {
        String strVal = "";
        int nCount = 0;
        while (value > 0)
        {
            strVal = (value % 10) + strVal;
            value /= 10;
            nCount++;
        }
        nCount = max - nCount;
        while (nCount > 0)
        {
            strVal = "0" + strVal;
            nCount--;
        }
        return strVal;
    }
    
    /**
     *
     */
     
    public static final int HOUR_TO_SECONDS(){
        return 3600;
    }
    
    public static int DAY_TO_SECONDS(){
        return 86400;
    }
    
    public static final long MILLISECONDS_OF_1_1_2010(){
        return 1264957200000L;
    }
    
    public static final long MILLISECONDS(){
        return (System.currentTimeMillis() - MILLISECONDS_OF_1_1_2010());
    }
    
    public static final int SECONDS(){
        return ((int)(MILLISECONDS()/1000));
    }
    
    /**
     *
     */
    public static final int IDX_DOW_INVALID             = 0;
    public static final int IDX_DOW_MONDAY              = 1;
    public static final int IDX_DOW_TUESDAY             = 2;
    public static final int IDX_DOW_WEDNESDAY           = 4;
    public static final int IDX_DOW_THURSDAY            = 8;
    public static final int IDX_DOW_FRIDAY              = 16;
    public static final int IDX_DOW_SATURDAY            = 32;
    public static final int IDX_DOW_SUNDAY              = 64;
    public static enum eCalendarOfWeek
    {
        Monday      (Calendar.MONDAY,          IDX_DOW_MONDAY,     "Monday",        0),
        Tuesday     (Calendar.TUESDAY,         IDX_DOW_TUESDAY,    "Tuesday",       1),
        Wednesday   (Calendar.WEDNESDAY,       IDX_DOW_WEDNESDAY,  "Wednesday",     2),
        Thursday    (Calendar.THURSDAY,        IDX_DOW_THURSDAY,   "Thursday",      3),
        Friday      (Calendar.FRIDAY,          IDX_DOW_FRIDAY,     "Friday",        4),
        Saturday    (Calendar.SATURDAY,        IDX_DOW_SATURDAY,   "Saturday",      5),
        Sunday      (Calendar.SUNDAY,          IDX_DOW_SUNDAY,     "Sunday",        6),
        ;
        private int nCalendarDay;
        private int nIndex;
        private int nOrder;
        private String strDesc;
        eCalendarOfWeek(int day, int index, String desc, int order)
        {
            nCalendarDay    = day;
            nIndex          = index;
            strDesc         = desc;
            nOrder          = order;
        }
    };
    public static final int getCurDayOfWeek() throws Exception
    {
        return getCurDayOfWeek(-1);
    }
    public static final int getCurDayOfWeek(long milliseconds) throws Exception
    {
        Calendar date = Calendar.getInstance();
        if (milliseconds > 0)
        {
            date.setTimeInMillis(milliseconds);
        }
        int dayInWeek = date.get(Calendar.DAY_OF_WEEK);
        for (eCalendarOfWeek day : eCalendarOfWeek.values())
        {
            if (dayInWeek == day.nCalendarDay)
            {
                return day.nIndex;
            }
        }
        return IDX_DOW_INVALID;
    }

    public static final String getDayOfWeekString(int dayOfWeek) throws Exception
    {
        for (eCalendarOfWeek day : eCalendarOfWeek.values())
        {
            if (dayOfWeek == day.nIndex)
            {
                return day.strDesc;
            }
        }
        return "";
    }

    /**
     *
     */
    public static int getDayOfWeekOrder(int nDOW) throws Exception
    {
        for (eCalendarOfWeek day : eCalendarOfWeek.values())
        {
            if (nDOW == day.nIndex)
            {
                return day.nOrder;
            }
        }
        return -1;
    }

    /**
     *
     */
    public static final long getDayOfWeekMilliseconds(int nDayOfWeek) throws Exception
    {
        int nCurDOW = getDayOfWeekOrder(getCurDayOfWeek(MILLISECONDS()));
        int nTarget = getDayOfWeekOrder(nDayOfWeek);
        long lMilliseconds = MILLISECONDS() - ((nCurDOW - nTarget) * DAY_TO_SECONDS() * 1000);
        Calendar date = Calendar.getInstance();
        Calendar ret = Calendar.getInstance();
        date.setTimeInMillis(lMilliseconds);
        ret.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        return ret.getTimeInMillis();
    }

    /**
     *
     */
    public static final int getSecondOfDay() throws Exception
    {
        return getSecondOfDay(-1);
    }
    public static final int getSecondOfDay(long milliseconds) throws Exception
    {
        Calendar date = Calendar.getInstance();
        if (milliseconds > 0)
        {
            date.setTimeInMillis(milliseconds);
        }
        return date.get(Calendar.HOUR_OF_DAY)*HOUR_TO_SECONDS() + date.get(Calendar.MINUTE)*60;
    }

    
    /**
     *  Save the file by binary array
     */
    public static boolean saveFile(String key, byte[] data)
    {
        try
        {
            if (data == null)
            {
                return false;
            }

            // Write new file
            FileOutputStream fos = new FileOutputStream(key);
            fos.write(data);
            fos.close();
            fos = null;
        }
        catch(Exception ex)
        {
            return false;
        }
        return true;
    }

    /**
     *  Save the file by string
     */
    public static boolean saveFile(String key, String str)
    {
        try
        {
            if (str == null)
            {
                return false;
            }

            // Write new file
            FileOutputStream fos = new FileOutputStream(key);
            fos.write(str.getBytes());
            fos.close();
            fos = null;
        }
        catch(Exception ex)
        {
            return false;
        }
        return true;
    }

    public static String readStringFromFile(String filePath) 
    {
        BufferedReader br = null;
        FileReader fr = null;
        StringBuilder sb = new StringBuilder();
        
        try {

            fr = new FileReader(filePath);
            br = new BufferedReader(fr);
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                sb.append(sCurrentLine).append("\n");
            }

        } catch (IOException e) {
            if(e.getMessage().indexOf("The system cannot find the file specified")!=-1)
                sb.append("The system cannot find the file specified");
            else{
                e.printStackTrace();
            }
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return sb.toString();
    }
    
    public static void writeStringToFile(String content, String filePath) 
    {
        File file = new File(filePath);
        Writer fileWriter = null;
        BufferedWriter bufferedWriter = null;
        try {
            fileWriter = new FileWriter(file, true);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(content);
        } catch (Exception e) {
        } finally {
            if (bufferedWriter != null && fileWriter != null) {
                try {
                    bufferedWriter.close();
                    fileWriter.close();
                } catch (Exception e) {
                }
            }
        }
    }
    
    public static void writeTextToFile(ArrayList<String> content, String filePath) 
    {
        File file = new File(filePath);
        Writer fileWriter = null;
        BufferedWriter bufferedWriter = null;
        try {
            fileWriter = new FileWriter(file);
            bufferedWriter = new BufferedWriter(fileWriter);
            // Write the lines one by one
            for (String line : content) {
                line += "\n";
                bufferedWriter.write(line);
    
            }
        } catch (Exception e) {
        } finally {
            if (bufferedWriter != null && fileWriter != null) {
                try {
                    bufferedWriter.close();
                    fileWriter.close();
                } catch (Exception e) {
                }
            }
        }
    }
    
    public static ArrayList<Integer> arrayToArrayListInteger(int[] params)
    {
        ArrayList<Integer> results = new ArrayList<Integer>();
        if(params == null || params.length == 0)
            return results;
        
        for(int i : params)
            results.add(i);
        return results;
    }
    
    public static ArrayList<String> arrayToArrayListString(String[] params)
    {
        ArrayList<String> results = new ArrayList<String>();
        if(params == null || params.length == 0)
            return results;
        
        for(String i : params){
            results.add(i);
        }
        return results;
    }
    
    public static String arrayIdxToString(int[] array)
    {
        if(array == null || array.length == 0)
            return "";
        
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < array.length; i++)
        {
            if(array[i] == 0)
                continue;
            sb.append(i).append(":").append(array[i]).append(";");
        }
        return sb.toString();
    }
    
    public static String arrayToString(ArrayList<Integer> array)
    {
        if(array == null || array.size() == 0)
            return "";
        
        StringBuilder sb = new StringBuilder();
        for(Integer i  : array)
        {
            sb.append(i.intValue()).append(";");
        }
        return sb.toString();
    }
    
    public static String convertToUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }
    
    public static String mergeString(CopyOnWriteArrayList<String> str, String seperator)
    {
        if(str == null || str.size() == 0){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < str.size(); i++)
        {
            sb.append(str.get(i));
            if(i < str.size() - 1)
                sb.append(seperator);
        }
        return sb.toString();
    }
    
    public static String[] explodeString(String baseStr, String seperator)
    {
        if(baseStr == null || baseStr == "")
            return null;
        return baseStr.split(seperator);
    }
    
     // HTTP Post request
    public static String sendingPostRequestWithUsernameAndPassword(String url, String postJsonData, String username, String password) throws Exception 
    {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        
        String auth = username+":"+password;
        byte[] encodedAuth = Base64.encodeToByte(auth.getBytes(StandardCharsets.UTF_8), false);
        String authHeaderValue = "Basic " + new String(encodedAuth);
        // Setting basic post request
        con.setRequestMethod("POST");
        con.setRequestProperty("Authorization", authHeaderValue);
        con.setRequestProperty("Accept","application/json");

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(postJsonData);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        String responseMsg = con.getResponseMessage();
        return responseCode + ":" + responseMsg;
        
        //System.out.println("nSending 'POST' request to URL : " + url);
        //System.out.println("Post Data : " + postJsonData);
        //System.out.println("Response Code : " + responseCode);
    }
    
     // HTTP Post request
    public static void sendingPostRequest(String url, String postJsonData, String authenKey) throws Exception 
    {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // Setting basic post request
        con.setRequestMethod("POST");
        if(authenKey!=null && authenKey != "")
            con.setRequestProperty("authentication", authenKey);
        con.setRequestProperty("Content-Type","application/json");

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(postJsonData);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        //System.out.println("nSending 'POST' request to URL : " + url);
        //System.out.println("Post Data : " + postJsonData);
        //System.out.println("Response Code : " + responseCode);
    }
    
    public static CompletableFuture<Integer> sendingPostRequestAsync(String url, String postJsonData, String authenKey) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                con.setRequestMethod("POST");
                if(authenKey!=null && !authenKey.isEmpty())
                    con.setRequestProperty("authentication", authenKey);
                con.setRequestProperty("Content-Type","application/json");

                con.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(postJsonData);
                wr.flush();
                wr.close();

                return con.getResponseCode();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }
    
    public static String genFacebookInstallBodyContent(String bundleVersion, String idfa)
    {
        ObjectMapper mapper = new ObjectMapper();
        
        ObjectNode objRootNode = mapper.createObjectNode();
        objRootNode.put("event", "MOBILE_APP_INSTALL");
        objRootNode.put("advertiser_id", idfa);
        objRootNode.put("advertiser_tracking_enabled", 1);
        objRootNode.put("application_tracking_enabled", 1);
        objRootNode.put("bundle_id", "com.vng.g6.a.zombie");
        objRootNode.put("bundle_version", bundleVersion);
        
        // System.out.println("[FB]:"+objRootNode.toString());
        
        return objRootNode.toString();
    }
    
    public static String genAppsflyerPurchaseBodyAndroid(String clientBundleVersion, String eventName, String paramRev, String appId, String idfa, String eventValue, String eventCurrency, String purchaseId, String deviceIp, String successCounter, String highestMission, String orderId) throws Exception
    {
        ObjectMapper mapper = new ObjectMapper();
        
        String evtValueStr = "{\""+paramRev+"\":\""+eventValue+"\" ,\"af_content_type\":\"wallets\",\"currency\":\""+eventCurrency+"\",\"af_content_id\":\""+purchaseId+"\",\"af_timelifesuccess\":\""+successCounter+"\",\"af_highestmission\":\""+highestMission+"\", \"af_client_version\":\""+clientBundleVersion+"\",\"af_order_id\":\""+orderId+"\"}";
        ObjectNode objRootNode = mapper.createObjectNode();
        objRootNode.put("appsflyer_id", appId);
        objRootNode.put("advertising_id", idfa);
        objRootNode.put("eventName", eventName);
        objRootNode.put("eventValue", evtValueStr);
        objRootNode.put("eventCurrency", eventCurrency);
        objRootNode.put("ip", deviceIp);
        objRootNode.put("eventTime", getCurrentUtcTime());
        objRootNode.put("af_events_api", "true");
        
        //System.out.println("Android:" + evtValueStr);
        
        return objRootNode.toString();
    }
    
    public static String genAppsflyerPurchaseBodyIOS(String clientBundleVersion, String eventName, String paramRev, String appId, String idfa, String eventValue, String eventCurrency, String purchaseId, String deviceIp, String successCounter, String highestMission, String orderId) throws Exception
    {
        ObjectMapper mapper = new ObjectMapper();
        
        String evtValueStr = "{\""+paramRev+"\":\""+eventValue+"\" ,\"af_content_type\":\"wallets\",\"currency\":\""+eventCurrency+"\",\"af_content_id\":\""+purchaseId+"\",\"af_timelifesuccess\":\""+successCounter+"\",\"af_highestmission\":\""+highestMission+"\", \"af_client_version\":\""+clientBundleVersion+"\",\"af_order_id\":\""+orderId+"\"}";
        ObjectNode objRootNode = mapper.createObjectNode();
        objRootNode.put("appsflyer_id", appId);
        objRootNode.put("idfa", idfa);
        objRootNode.put("bundle_id", "com.vng.g6.a.zombie");
        objRootNode.put("eventName", eventName);
        objRootNode.put("eventValue", evtValueStr);
        objRootNode.put("eventCurrency", eventCurrency);
        objRootNode.put("ip", deviceIp);
        objRootNode.put("eventTime", getCurrentUtcTime());
        objRootNode.put("af_events_api", "true");
        
        //System.out.println("IOS:" + evtValueStr);
        
        return objRootNode.toString();
    }    
    
    public static String dateFormation = "yyyy-MM-dd hh:mm:ss.SSS";
    
    public static String parseTimeToString(Date date, String zone, Locale locale)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormation, locale);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(zone));
        return simpleDateFormat.format(date);
    }
    
    public static String getCurrentUtcTime() throws Exception
    {
        return parseTimeToString(new Date(), "UTC", Locale.US);
    }
    
    public static long getCurrentUtcTimeInUnix() throws Exception
    {
        SimpleDateFormat localDateFormat = new SimpleDateFormat(dateFormation);
        Date d = localDateFormat.parse(getCurrentUtcTime());
        return d.getTime() / 1000;
    }
    
    // Only for log ifrs, because it the same title of file log4j write down
    public static long getCrtServerTimeInUnix()
    {
        Calendar cal = Calendar.getInstance(); 
        return cal.getTimeInMillis() / 1000;
    }
    
    public static boolean writeFile(String filename, String content) throws Exception
    {
        return writeFile(filename, content, false);
    }
    public static boolean writeFile(String filename, String content, boolean isJsonFormat) throws Exception
    {
        try {
            //System.out.println("WRITEFILE: "+content);
            FileWriter writer = new FileWriter(filename, false);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            int length = 0;
            if(isJsonFormat)
            {
                length = content.length();
                bufferedWriter.write(content, 0, length);
            } else {
                String[] splitContents = content.split(";");
                for(int i = 0; i < splitContents.length; i++)
                {
                    length = splitContents[i].length();
                    bufferedWriter.write(splitContents[i], 0, length);
                    if(i<splitContents.length-1)
                        bufferedWriter.newLine();
                }
            }
            bufferedWriter.close();
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public static String readFile(String filename) throws Exception
    {
        return readFile(filename, true);
    }
    
    public static String readFile(String filename, boolean needSymbolEndline) throws Exception
    {
        
        if(new File(filename).exists())
        {
            try {
                StringBuilder sb = new StringBuilder();
                FileReader reader = new FileReader(filename);
                BufferedReader bufferedReader = new BufferedReader(reader);
                
                String tmpStr = "";
                while((tmpStr=bufferedReader.readLine())!=null)
                {
                    sb.append(tmpStr);
                    if(needSymbolEndline)
                        sb.append(";");
                }
                
                bufferedReader.close();
                reader.close();
                //System.out.println("READFILE: "+sb.toString());
                return sb.toString();
                
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        return null;
    }
}
