package firebat.utils;

import java.security.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

//import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
/**
 * Apple IAP and Google Play In-app Billing Verification Class
 *
 */
public class AppleSign {
    //static Logger logger = Logger.getLogger(IapVerification.class);
    public static final String VERIFICATION_URL_REAL = "https://buy.itunes.apple.com/verifyReceipt";
    public static final String VERIFICATION_URL_SANDBOX = "https://sandbox.itunes.apple.com/verifyReceipt";
    /**
     * @return int
     * @param receipt
     * @param isTest
     * @return 0: fail, 1: success, -1 : urus hacking
     * @throws IOException
     */
	public static String verifyApplePurchase(String receipt, boolean isTest) throws IOException {
		String returnedString = null;
		if (isTest)
		{
			returnedString = verifyAppleReceiptData(VERIFICATION_URL_SANDBOX, receipt);
		} else {
			returnedString = verifyAppleReceiptData(VERIFICATION_URL_REAL, receipt);
		}
				
		ObjectMapper mapper = new ObjectMapper();
		JsonNode resultObject = mapper.readTree(returnedString);
		int resultStatus = Integer.parseInt(resultObject.get("status").toString());
		if (resultStatus != 0)
		{
			// 21000: The App Store could not read the JSON object you provided.
			// 21002: The data in the receipt-data property was malformed or missing.
			// 21003: The receipt could not be authenticated.
			// 21004: The shared secret you provided does not match the shared secret on file for your account.
			// 21005: The receipt server is not currently available.
			// 21006: This receipt is valid but the subscription has expired. When this status code is returned to your server, the receipt data is also decoded and returned as part of the response.
				// Only returned for iOS 6 style transaction receipts for auto-renewable subscriptions.
			// 21007: This receipt is from the test environment, but it was sent to the production environment for verification. Send it to the test environment instead.
			// 21008: This receipt is from the production environment, but it was sent to the test environment for verification. Send it to the production environment instead.
			// 21010: This receipt could not be authorized. Treat this the same as if a purchase was never made.
			// 21100-21199: Internal data access error.
			if(resultStatus == 21007) // try to sandbox
			{
				returnedString = verifyAppleReceiptData(VERIFICATION_URL_SANDBOX, receipt);
				ObjectMapper mapper2 = new ObjectMapper();
				JsonNode resultObject2 = mapper2.readTree(returnedString);
				resultStatus = Integer.parseInt(resultObject2.get("status").toString());
				if(resultStatus != 0) // Error again
					return null;
			} else {
				return null;
			}
		}
		return returnedString;
	}
	
	/**
	* @return String
	* @param urladdress
	* @param receiptData
	* @return result string
	* @throws IOException
	*/
	private static String verifyAppleReceiptData(String urladdress, String receiptData) throws IOException  
	{
		URL url = null;
		URLConnection conn = null;
		OutputStreamWriter osw = null;
		BufferedReader br = null;
		StringBuffer sb = new StringBuffer();

		try {
			String jsonData = "{" +	"\"receipt-data\" : \"" + receiptData + "\"," +	"}";
			url = new URL(urladdress);
			conn = url.openConnection();
			conn.setDoOutput(true);
			osw = new OutputStreamWriter(conn.getOutputStream());
			osw.write(jsonData);
			osw.flush();

			// Get the response
			br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			sb = new StringBuffer();
			while ((line = br.readLine()) != null) {
				// Process line...
				sb.append(line);
			}
		} finally {
			if (osw != null)
				osw.close();
			if (br != null)
				br.close();
		}

		return sb.toString();
	}
}
