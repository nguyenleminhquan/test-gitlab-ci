package firebat.utils;

import java.lang.management.ManagementFactory;
import com.sun.management.OperatingSystemMXBean;

public class Monitor {
    private final static int MEGABYTE = 1048576;
    private static int availableProcessors;
    private static long heapMemoryTotal = -1;
    private static OperatingSystemMXBean bean;

    static {
        bean = (com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        availableProcessors = Runtime.getRuntime().availableProcessors();
        heapMemoryTotal = Runtime.getRuntime().totalMemory();
    }

    // **CPUs**
    public static int availableProcessors() {
        return availableProcessors;
    }

    public static double getProcessCpuUsedPercent() {
        return (int) (bean.getProcessCpuLoad() * 100);
    }

    public static double getSystemCpuUsedPercent() {
        return (int) (bean.getSystemCpuLoad() * 100);
    }
    // **Memory**

    // Physical Mem
    public static int getPhysicalMemTotal() {
        return (int) (bean.getTotalPhysicalMemorySize() / MEGABYTE);
    }

    public static int getPhysicalMemUsed() {
        return (int) ((bean.getTotalPhysicalMemorySize() - bean.getFreePhysicalMemorySize()) / MEGABYTE);
    }

    public static int getPhysicalMemUsedPercent() {
        float totalMem = (float) bean.getTotalPhysicalMemorySize();
        float freeMem = (float) bean.getFreePhysicalMemorySize();
        return (int) (((totalMem - freeMem) / totalMem) * 100);
    }

    // Heap Mem
    public static int getHeapMemoryTotal() {
        return (int) (Runtime.getRuntime().totalMemory() / MEGABYTE);
    }

    public static int getHeapMemoryUsed() {
        return (int) ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / MEGABYTE);
    }

    public static int getMemoryUsedPercent() {
        long freeMem = Runtime.getRuntime().freeMemory();
        return (int) (((float) (heapMemoryTotal - freeMem) / heapMemoryTotal) * 100);
    }

}
