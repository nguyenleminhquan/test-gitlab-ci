package firebat.utils;

import firebat.log.Log;

public class ServiceStatus
{    
    private final static int NOT_STARTED      = 0;    
    private final static int STARTING_UP      = 1;    
    private final static int STARTED          = 2;
    private final static int SHUTTING_DOWN    = 3;
    private final static int SHUTDOWN         = 4;    
    
    private final static String datePattern = "yyyy-MM-dd HH:mm:ss";
    
    private int status = NOT_STARTED;    
    private final boolean useMark;
    
    public ServiceStatus ()
    {
        this(false);
    }
    
    public ServiceStatus (boolean useMark)
    {
        this.useMark = useMark;
    }
    
    public void set (int status)
    {
        this.status = status;
    }
    
    public int get ()
    {
        return status;
    }
    
    public void setException ()
    {
        if (useMark) Log.console(Time.currentDateString(datePattern), "MARK_SERVICE_HAS_EXCEPTION", status);
        status = (status > 0) ? -status : status;        
    }
    
    public boolean isException ()
    {
        return status < 0;
    }
    
    public void setNotStarted ()
    {
        status = NOT_STARTED;
        if (useMark) Log.console(Time.currentDateString(datePattern), "MARK_SERVICE_STATUS_NOT_STARTED");
    }
    
    public boolean isNotStarted ()
    {
        return status == NOT_STARTED;
    }
    
    public void setStartingUp ()
    {
        status = STARTING_UP;
        if (useMark) Log.console(Time.currentDateString(datePattern), "MARK_SERVICE_STATUS_STARTING_UP");
    }
    
    public boolean isStartingUp ()
    {
        return status == STARTING_UP;
    }
    
    public void setStarted ()
    {
        status = STARTED;
        if (useMark) Log.console("MARK_SERVICE_STATUS_STARTED");
    }
    
    public boolean isStarted ()
    {
        return status == STARTED;
    }
    
    public void setShuttingDown ()
    {
		/// Update const before shutdown
		// Log.debug("Write file before shutting down");
		// Const.constGiftCodes.saveRemainGiftCodes();
		
        status = SHUTTING_DOWN;
        if (useMark) Log.console("MARK_SERVICE_STATUS_SHUTTING_DOWN");
    }
    
    public boolean isShuttingDown ()
    {
        return status == SHUTTING_DOWN;
    }
    
    public void setShutdown ()
    {
        status = SHUTDOWN;
        if (useMark) Log.console(Time.currentDateString(datePattern), "MARK_SERVICE_STATUS_SHUTDOWN");
    }
    
    public boolean isShutdown ()
    {
        return status == SHUTDOWN;
    }
}