package firebat.utils;

import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.nio.charset.Charset;
public class AndroidSign
{
	private static final String KEY_FACTORY_ALGORITHM = "RSA";
    private static final String SIGNATURE_ALGORITHM = "SHA1withRSA";

    /**
     * Verifies that the data was signed with the given signature, and returns
     * the verified purchase. The data is in JSON format and signed
     * with a private key. The data also contains the {@link PurchaseState}
     * and product ID of the purchase.
     * @param base64PublicKey the base64-encoded public key to use for verifying.
     * @param signedData the signed JSON string (signed, not encrypted)
     * @param signature the signature for the data, signed with the private key
     */
    public static boolean verifyPurchase(String signature,String signedData, String base64PublicKey) throws Exception {
        if (signedData == null) {
            return false;
        }

        if (signature!=null) 
		{
            PublicKey key = generatePublicKey(base64PublicKey);
			if(key == null){
				return false;
			}
            return verify(key, signedData, signature);
        }
        return true;
    }

    /**
     * Generates a PublicKey instance from a string containing the
     * Base64-encoded public key.
     *
     * @param encodedPublicKey Base64-encoded public key
     * @throws IllegalArgumentException if encodedPublicKey is invalid
     */
    public static PublicKey generatePublicKey(String encodedPublicKey) throws Exception {
        try {
            byte[] decodedKey = Base64.decode(encodedPublicKey.toCharArray());
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_FACTORY_ALGORITHM);
            return keyFactory.generatePublic(new X509EncodedKeySpec(decodedKey));
        } catch (Exception e) {
			System.out.println(e.getMessage());
        }
		return null;
    }

    /**
     * Verifies that the signature from the server matches the computed
     * signature on the data.  Returns true if the data is correctly signed.
     *
     * @param publicKey public key associated with the developer account
     * @param signedData signed data from server
     * @param signature server signature
     * @return true if the data and signature match
     */
    public static boolean verify(PublicKey publicKey, String signedData, String signature) throws Exception {
        Signature sig;
        try {
            sig = Signature.getInstance(SIGNATURE_ALGORITHM);
            sig.initVerify(publicKey);
            sig.update(signedData.getBytes());
            return sig.verify(Base64.decode(signature.toCharArray()));
        } catch (Exception e) {
        }
        return false;
    }
}
