package firebat.db.memcached;

import com.couchbase.client.java.json.JsonObject;

public class DocumentTest extends Document {
    public String dbName;
    public int numRetry;
    public long timeCreated;

    public DocumentTest(JsonObject jsonObject) {
        dbName = jsonObject.getString("Database Name");
        numRetry = jsonObject.getInt("Num retry");
        timeCreated = jsonObject.getLong("Time create");
    }

    @Override
    public JsonObject toJson() {
        return JsonObject.create()
                .put("Database Name", dbName)
                .put("Num retry", numRetry)
                .put("Time create", timeCreated);
    }
}
