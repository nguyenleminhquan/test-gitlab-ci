package firebat.db.memcached;

import com.couchbase.client.java.*;
import com.couchbase.client.java.kv.*;

import firebat.log.Log;

import com.couchbase.client.core.error.*;
import com.couchbase.client.java.json.JsonObject;

import static com.couchbase.client.java.kv.GetOptions.getOptions;
import static com.couchbase.client.java.kv.InsertOptions.insertOptions;
import static com.couchbase.client.java.kv.ReplaceOptions.replaceOptions;
import static com.couchbase.client.java.kv.UpsertOptions.upsertOptions;

import java.time.Duration;

public class SpyMemcached extends AbstractDbKeyValue {
    private Cluster cluster;
    private Bucket bucket;
    private String bucketIP;
    private String bucketName;
    private com.couchbase.client.java.Collection collection;

    public SpyMemcached(String bucketIP, String bucketName, String bucketUserName, String bucketPassword) {
        this.bucketIP = bucketIP;
        this.bucketName = bucketName;

        cluster = Cluster.connect(bucketIP, bucketUserName, bucketPassword);

        bucket = cluster.bucket(bucketName);
        bucket.waitUntilReady(Duration.ofSeconds(10));

        collection = bucket.defaultCollection();
    }

    @Override
    public void disconnect() {
        cluster.disconnect();
    }

    @Override
    public String getName() {
        return bucketIP + ":" + bucketName;
    }

    @Override
    public boolean exist(String key) {
        ExistsResult result = collection.exists(key);
        return result.exists();
    }

    @Override
    public boolean set(String key, Document value) {
        return set(key, value, NO_EXPIRATION);
    }

    @Override
    public boolean set(String key, Document value, long expiration) {
        try {
            expiration = modifyExpiration(expiration);
            if(expiration <= NO_EXPIRATION) {
                MutationResult insertResult = collection.insert(key, value.toJson());
            }
            else {
                MutationResult insertResult = collection.insert(key, value.toJson(),
                        insertOptions().expiry(Duration.ofSeconds(expiration)));
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public <T extends Document> T get(Class<T> type, String key) {
        if (!exist(key)) {
            Log.debug("Key not exist: " + key);
            return null;
        }
        try {
            GetResult getResult = collection.get(key);
            return Document.fromJson(type, getResult.contentAsObject());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean delete(String key) {
        try {
            if (!exist(key)) {
                Log.debug("Key not exist: " + key);
                return false;
            }
            collection.remove(key);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean replace(String key, Document value) {
        return replace(key, value, NO_EXPIRATION);
    }

    @Override
    public boolean replace(String key, Document value, long expiration) {
        try {
            expiration = modifyExpiration(expiration);
            if (expiration <= NO_EXPIRATION) {
                MutationResult replaceResult = collection.upsert(key, value.toJson());
            } else {
                MutationResult replaceResult = collection.upsert(key, value.toJson(),
                        upsertOptions().expiry(Duration.ofSeconds(expiration)));
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
