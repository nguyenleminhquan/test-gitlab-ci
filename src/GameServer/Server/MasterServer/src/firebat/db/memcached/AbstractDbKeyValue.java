package firebat.db.memcached;

import firebat.utils.Time;
import com.couchbase.client.java.json.JsonObject;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Map;

public abstract class AbstractDbKeyValue
{
    public final static int NO_EXPIRATION       = 0;
    public final static int EXPIRE_IN_MINUTE    = Time.SECOND_IN_MINUTE;
    public final static int EXPIRE_IN_HOUR      = Time.SECOND_IN_HOUR;
    public final static int EXPIRE_IN_DAY       = Time.SECOND_IN_DAY;
    public final static int EXPIRE_IN_7_DAY     = Time.SECOND_IN_7_DAY;
    public final static int EXPIRE_IN_30_DAY    = Time.SECOND_IN_30_DAY;
    
    public final static String TEST_KEY         = "TEST_DATABASE_CONNECTION_KEY";
    private final static int MIN_EXPIRATION     = (int) ((new GregorianCalendar(2014, 1 - 1, 1)).getTimeInMillis() / 1000);

    public abstract void disconnect ();

    public abstract String getName ();

    public abstract boolean exist(String key);

    public abstract boolean set(String key, Document value);

    public abstract boolean set(String key, Document value, long expiration);

    public abstract <T extends Document> T get(Class<T> type, String key);

    public abstract boolean delete(String key);

    public abstract boolean replace(String key, Document value);

    public abstract boolean replace(String key, Document value, long expiration);

    public static long modifyExpiration (long expiration)
    {
        if (expiration <= NO_EXPIRATION)
            return NO_EXPIRATION;
        if (expiration <= MIN_EXPIRATION)
            return expiration + (long) Time.currentTimeSecond();
        return expiration;
    }
}
