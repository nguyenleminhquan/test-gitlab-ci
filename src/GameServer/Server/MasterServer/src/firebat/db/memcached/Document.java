package firebat.db.memcached;

import com.couchbase.client.java.json.JsonObject;

public abstract class Document {
    public static <T extends Document> T fromJson(Class<T> documentClass, String jsonStr) {
        JsonObject jsonObject = JsonObject.fromJson(jsonStr);
        return Document.fromJson(documentClass, jsonObject);
    }

    public static <T extends Document> T fromJson(Class<T> documentClass, JsonObject jsonObject) {
        try {
            T document = documentClass.getDeclaredConstructor(JsonObject.class).newInstance(jsonObject);
            return document;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return this.toJson().toString();
    }

    public abstract JsonObject toJson();
}
