package firebat.log;

import firebat.utils.Environment;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.FileInputStream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class Log {
    public final static int INIT_CAPACITY = 256;
    public final static char LOG_IFRS_SEPARATOR_COLUMN = ',';
    public final static char LOG_GAME_SEPARATOR_COLUMN = '\t';

    private static boolean useDebug = true;

    public static Logger    loggerConsole;
    public static Logger    loggerException;
    public static Logger    loggerIfrs;

    public synchronized static void start(Environment env) throws Exception {
        String folder = "./data/";
        String fileProperties;

        switch (env) {
            case SERVER_LIVE:
                fileProperties = "logServerLive.properties";
                break;
            case DEV:
                fileProperties = "logDev.properties";
                break;
            default:
                throw new Exception("Not support log type " + env.name());
        }

        start(folder + fileProperties, env == Environment.DEV);
    }

    public synchronized static void start(String filePath, boolean isDebug) throws Exception {
        useDebug = isDebug;
        
        ConfigurationSource source = new ConfigurationSource(new FileInputStream(filePath), new File(filePath));
        Configurator.initialize(null, source);
        
        loggerConsole = LogManager.getRootLogger();
        loggerException = LogManager.getLogger("EXCEPTION");
        loggerIfrs = LogManager.getLogger("IFRS");
        
        System.out.println("Use " + filePath + "; " + (loggerException!=null));
    }

    public synchronized static void stop() {
        LogManager.shutdown();
        
        loggerConsole = null;
        loggerException = null;
        loggerIfrs = null;
    }

    public static CharSequence getCharSequence(char separator, Object... acs) {
        return getCharSequence(new StringBuilder(INIT_CAPACITY), separator, acs);
    }

    public static CharSequence getCharSequence(StringBuilder sb, char separator, Object... acs) {
        for (Object o : acs) {
            sb.append(o);
            if (separator > 0) {
                sb.append(separator);
            }
        }
        return sb;
    }

    public static void debug(Object... acs) {
        if (useDebug) {
            console(acs);
        }
    }

    public static void console(Object... acs) {
        CharSequence msg = getCharSequence(LOG_GAME_SEPARATOR_COLUMN, acs);
        if (loggerConsole == null) {
            System.out.println(msg);
        } else {
            loggerConsole.info(msg);
        }
    }

    public static void info(Object... acs) {
        CharSequence msg = getCharSequence(LOG_GAME_SEPARATOR_COLUMN, acs);
        console(msg);
    }

    public static void exception (Throwable cause, Object... acs) {
        if (loggerException == null) {
            cause.printStackTrace();
        } else {
            StringBuilder sb = new StringBuilder(INIT_CAPACITY);
            if (acs.length > 0) {
                sb.append(getCharSequence(sb, LOG_GAME_SEPARATOR_COLUMN, acs));
            }
            sb.append(cause.toString()).append('\n');
            for (StackTraceElement ste : cause.getStackTrace()) {
                sb.append(ste.toString()).append('\n');
            }
            loggerException.info(sb);
        }
    }

    public static void ifrs (
            String account,
            long unixTime,
            float chargeCoinExchange,
            Object itemId, //CharSequence itemId,
            String actionId,
            float unitPrice
    ) {
        ifrs(account, unixTime, chargeCoinExchange, itemId, actionId, unitPrice, "USD");
    }
	
	public static void ifrs (
            String account,
            long unixTime,
            float chargeCoinExchange,
            Object itemId, //CharSequence itemId,
            String actionId,
            float unitPrice,
            String currency
    ) {
        StringBuilder sb = new StringBuilder(INIT_CAPACITY)
                .append(account).append(LOG_GAME_SEPARATOR_COLUMN)
                .append("0.00").append(LOG_GAME_SEPARATOR_COLUMN)
                .append("0.00").append(LOG_GAME_SEPARATOR_COLUMN)
                .append(unixTime).append(LOG_GAME_SEPARATOR_COLUMN)
                .append(chargeCoinExchange).append(LOG_GAME_SEPARATOR_COLUMN)
                .append("0.00").append(LOG_GAME_SEPARATOR_COLUMN)
                .append(itemId).append(LOG_GAME_SEPARATOR_COLUMN)
                .append(actionId).append(LOG_GAME_SEPARATOR_COLUMN)
                .append(unitPrice).append(LOG_GAME_SEPARATOR_COLUMN)
                .append(unitPrice).append(LOG_GAME_SEPARATOR_COLUMN)
                .append((unitPrice*7/10)).append(LOG_GAME_SEPARATOR_COLUMN)
                .append(currency).append(LOG_GAME_SEPARATOR_COLUMN);
		
        loggerIfrs.info(sb);
    }
}
