#!/bin/bash

source ./config.sh $1

echo "Remove old sources gen"
if [ -d "$DIR_MPS_SRC_GEN" ]; then
  rm -rf "$DIR_MPS_SRC_GEN"
fi

echo "Export new protos"
cd $DIR_DATA
for proto in * ; do
  FILE_NAME=$(basename -- "$proto")
  FILE_EXT="${FILE_NAME##*.}"
  if [ "$FILE_EXT" = "proto" ]; then
    $PROTO_COMPILER --proto_path=$DIR_DATA --java_out=$DIR_SRC $DIR_DATA/$FILE_NAME
  fi
done

echo "Protos completed!"