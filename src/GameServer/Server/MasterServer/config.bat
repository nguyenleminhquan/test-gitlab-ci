if "%SYSTEM_DEBUG%"=="true" echo on
:: set JAVA_HOME=C:\DevTools\jdk1.7.0_05

FOR /F %%i IN (version.txt) DO SET BUILD_VERSION=%%i
FOR /F %%i IN (clientVersion.txt) DO SET BS_CLIENT_CURRENT_VERSION=%%i

set JAR_NAME=LetGetRich_%BUILD_VERSION%.jar
set MAIN_CLASS=projectx.Main

REM set ENVIRONMENT=SERVER_LIVE
if "%ENVIRONMENT%"=="" set ENVIRONMENT=DEV

set PRJ_HOME=%cd%
set DIR_DATA=%PRJ_HOME%\..\..\ProtocolData
set DIR_LIB=%PRJ_HOME%\lib
set DIR_SRC=%PRJ_HOME%\src
set DIR_INFO=%PRJ_HOME%\info
set DIR_TOOL=%PRJ_HOME%\tools
set DIR_MPS_SRC_GEN=%DIR_SRC%\projectx\protocols
set DIR_SRC_GAME=%DIR_SRC%\projectx

set LIBRARY=%DIR_LIB%\io\netty-all-4.0.25.Final.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\io\httpcore-4.3.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\io\httpcore-nio-4.3.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\log\log4j-core-2.20.0.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\log\log4j-api-2.20.0.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\log\genCodeScribe.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\memcached\couchbase\java-client-3.4.3.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\memcached\couchbase\core-io-2.4.3.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\memcached\couchbase\reactive-streams-1.0.4.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\memcached\couchbase\reactor-core-3.5.0.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\memcached\spymemcached-2.11.5.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\misc\commons-codec-1.5.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\misc\gson-2.10.1.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\misc\guava-15.0.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\misc\jettison-1.1.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\misc\libthrift-0.9.1.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\misc\jackson-all-1.9.9.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\misc\json-20220320.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\misc\protobuf-java-3.22.2.jar
set LIBRARY=%LIBRARY%;%DIR_LIB%\jdbc\bonecp-0.8.0.RELEASE.jar


set DIR_TEMP=%PRJ_HOME%\_temp_
set DIR_TEMP_SRC=%DIR_TEMP%\0_src
set DIR_TEMP_CLASS=%DIR_TEMP%\1_class

set DIR_RELEASE=%PRJ_HOME%\_release_
set DIR_RELEASE_DATA=%DIR_RELEASE%\data
set DIR_RELEASE_LIB=%DIR_RELEASE%\lib

set JAVA="%JAVA_HOME%\bin\java"
set JAVAC="%JAVA_HOME%\bin\javac"
set JAR="%JAVA_HOME%\bin\jar"
set PROTO_COMPILER="%DIR_TOOL%\protocolbuf\windows\bin\protoc.exe"

:: Override config with local settings
SET LOCAL_CONFIG=%~nx0.local
SET TEMP_SCRIPT=%DIR_TEMP%\temp.cmd
if exist "%LOCAL_CONFIG%" (
	type "%LOCAL_CONFIG%" > "%TEMP_SCRIPT%"
	call "%TEMP_SCRIPT%"
	set
)

call define.bat


