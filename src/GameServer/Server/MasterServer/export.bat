@echo off
setlocal enabledelayedexpansion

echo.
echo Init export tool ...
call config.bat

echo Remove old sources gen ...
if exist "%DIR_MPS_SRC_GEN%" (
    rd /s /q "%DIR_MPS_SRC_GEN%"
)

echo Export new protos ...

for /F "delims=" %%a in ('dir "%DIR_DATA%" /b') do (
    set FILE_NAME=%%a
    set FILE_EXT=%%~xa
    if "!FILE_EXT!" == ".proto" (
        call %PROTO_COMPILER% -I=%DIR_DATA% --java_out=%DIR_SRC% %DIR_DATA%/!FILE_NAME!
    )
)

echo Protos completed!