package projectx.event;

import firebat.log.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * @author HuyNH4
 */
public class EventManager
{
    public static final String dateFormat = "dd/MM/yyyy HH:mm:ss";
    
    private static final String eventInfoFilePath = "./data/eventInfo.txt";
    private static final String eventScheduleFilePath = "./data/eventSchedule.txt";
    private static final ScheduledExecutorService scheduledLoopEvent = Executors.newSingleThreadScheduledExecutor();
    private static ArrayList<EventInfo> eventInfos = new ArrayList<EventInfo>();
    private static Calendar firstStart = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    
    public static EventInfo eventInfo = null;
    
    public static void init()
    {
        loadEventData();
        scheduleEvent();
    }
    
    private static void loadEventData()
    {
        System.out.println("--- Load Event Info");
        if (new File(eventInfoFilePath).exists())
        {
            try
            {
                FileReader reader = new FileReader(eventInfoFilePath);
                BufferedReader bufferedReader = new BufferedReader(reader);
                
                // Get event info
                String[] ids = bufferedReader.readLine().split("=");
                if (ids != null && ids.length == 2)
                {
                    String[] infos = ids[1].split(";");
                    if (infos != null && infos.length > 0)
                    {
                        for (String info : infos)
                        {
                            EventInfo eventInfo = new EventInfo(info);
                            eventInfos.add(eventInfo);
                            System.out.println("--- --- Event:" + eventInfo.toString());
                        }
                    }
                }

                // Get start time
                String[] start = bufferedReader.readLine().split("=");
                if (start != null && start.length == 2)
                {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
                    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                    try
                    {
                        firstStart.setTime(simpleDateFormat.parse(start[1]));
                    }
                    catch (ParseException e)
                    {
                        firstStart.set(2023, 5, 12, 13, 0, 0); // month is 0-base: Jun = 6 - 1 = 5
                    }
                    System.out.println("--- --- Start:" + firstStart.toInstant());
                }

                bufferedReader.close();
                reader.close();
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
            }
        }
    }
    
    public static void scheduleEvent()
    {
        System.out.println("--- Schedule Event Loop");
        int index = 0;
        if (eventInfo == null)
        {
            if (new File(eventScheduleFilePath).exists())
            {
                try
                {
                    FileReader reader = new FileReader(eventScheduleFilePath);
                    BufferedReader bufferedReader = new BufferedReader(reader);

                    String id = bufferedReader.readLine();
                    String start = bufferedReader.readLine();

                    for (index = 0; index < eventInfos.size(); index++)
                    {
                        if (eventInfos.get(index).eventId.equals(id))
                        {
                            eventInfo = eventInfos.get(index);
                            eventInfo.setupTime(start);
                            break;
                        }
                    }

                    bufferedReader.close();
                    reader.close();
                }
                catch (IOException e)
                {
                    System.out.println(e.getMessage());
                }
            }
        }
        
        if (eventInfo == null)
        {
            index = 0;
            eventInfo = eventInfos.get(index);
            eventInfo.setupTime(firstStart.getTime());            
        }
        else
        {
            index = eventInfos.indexOf(eventInfo);
        }
        
        Calendar startTime = eventInfo.startTime;
        while (eventInfo.isPassed())
        {
            index++;
            if (index >= eventInfos.size())
            {
                index = 0;
            }
            startTime.add(Calendar.MINUTE, eventInfo.durationInMinute + eventInfo.breakTimeInMinute);
            eventInfo = eventInfos.get(index);
            eventInfo.setupTime(startTime.getTime());
        }

        saveSchedule(eventInfo);
        Log.debug("--- --- id    :" + eventInfo.eventId);
        Log.debug("--- --- start :" + eventInfo.startTime.toInstant());
        Log.debug("--- --- end   :" + eventInfo.endTime.toInstant());
        Log.debug("--- --- remain:" + eventInfo.remainingTimeInSecond());
        Log.debug("--- --- break :" + eventInfo.breakTimeInMinute);

        scheduledLoopEvent.schedule(new EventLoopSchedule(), eventInfo.remainingTimeInSecond() + 1, java.util.concurrent.TimeUnit.SECONDS); //+1: make sure event has ended
    }
    
    public static void saveSchedule(EventInfo eventInfo)
    {
        try
        {
            FileWriter writer = new FileWriter(eventScheduleFilePath, false);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            
            bufferedWriter.write(eventInfo.eventId);
            bufferedWriter.newLine();
            
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            bufferedWriter.write(simpleDateFormat.format(eventInfo.startTime.getTime()));
            
            bufferedWriter.close();
            writer.close();
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
