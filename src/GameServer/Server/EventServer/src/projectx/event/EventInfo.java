package projectx.event;

import firebat.utils.Misc;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by huynh4
 */
public class EventInfo
{
	public String eventId;
    public Calendar startTime;
    public Calendar endTime;
	public int durationInMinute;
	public int breakTimeInMinute;
    
    public EventInfo(String info)
    {
        String[] data = info.split(":");
        if (data != null && data.length == 3)
        {
            try
            {
                eventId = data[0];
                durationInMinute = Integer.parseInt(data[1]);
                breakTimeInMinute = Integer.parseInt(data[2]);
            }
            catch (Exception e)
            {
                eventId = null;
            }
        }
    }
    
    public EventInfo(String id, int duration, int breaktime)
    {
        eventId = id;
        durationInMinute = duration;
        breakTimeInMinute = breaktime;
    }
    
    public void setupTime(String source)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(EventManager.dateFormat);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try
        {
            setupTime(simpleDateFormat.parse(source));
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    public void setupTime(Date date)
    {
        startTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        startTime.setTime(date);
        
        endTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        endTime.setTime(date);
        endTime.add(Calendar.MINUTE, durationInMinute);
    }
    
    public void setupTime(int year, int month, int date, int hourOfDay, int minute, int second) // month is 0-base
    {
        startTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        startTime.set(year, month, date, hourOfDay, minute, second);
        
        endTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        endTime.set(year, month, date, hourOfDay, minute, second);
        endTime.add(Calendar.MINUTE, durationInMinute);
    }
    
    public String startTimeInString()
    {
        return Misc.parseTimeToString(startTime.getTime(), "UTC", Locale.US);
    }
    
    public long remainingTimeInSecond()
    {
        return endTime.getTimeInMillis() / 1000 - Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis() / 1000;
    }
    
    public boolean isPassed()
    {
        return endTime.getTimeInMillis() < Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
    }
    
    public String toString()
    {
        return eventId + ":" + durationInMinute + ":" + breakTimeInMinute;
    }
}