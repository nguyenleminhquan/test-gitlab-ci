package projectx.database;

import java.util.Map;
import java.util.Set;
import java.util.List;
import firebat.log.Log;
import projectx.Main;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.resps.Tuple;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

public class Database
{
    // DB defines
    public static final String DEFAULT_HOST = "localhost";
    public static final int DEFAULT_PORT = 6379;
    // Static params
    public static final int SORT_PARAM_ASC = 0;
    public static final int SORT_PARAM_DEC = 1;

    // Static instance
    private static JedisPool jedisPool = null;

    public static void init(String host, String port, String pass)
    {
        Log.info("DBREDIS: Establish connection...");
        String dbHost = host == null || host.isEmpty() ? DEFAULT_HOST : host;
        int dbPort = port == null || port.isEmpty() ? DEFAULT_PORT : Integer.parseInt(port);
        if (pass == null || pass.isEmpty())
            jedisPool = new JedisPool(getDefaultConfigs(), dbHost, dbPort);
        else
            jedisPool = new JedisPool(getDefaultConfigs(), dbHost, dbPort, 5, pass);
        // Ping signal
        Log.info("DBREDIS: Conneted! (host:" + dbHost + ";port:" + dbPort + ") => " + pingSignal());
    }

    // config specific pool
    public static GenericObjectPoolConfig<Jedis> getDefaultConfigs()
    {
        GenericObjectPoolConfig<Jedis> cf = new GenericObjectPoolConfig<Jedis>();
        cf.setTestWhileIdle(true);
        cf.setMinEvictableIdleTimeMillis(60000);
        cf.setTimeBetweenEvictionRunsMillis(30000);
        cf.setNumTestsPerEvictionRun(-1);
        cf.setMaxTotal(50);
        return cf;
    }

    public static void close()
    {
        jedisPool.close();
    }

    public static String pingSignal()
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        ///it's important to return the Jedis instance to the pool once you've finished using it
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            String ping = jedis.ping();
            jedis.close();
            return ping;
        }
        else
        {
            System.out.println("No Database");
        }
        return "NoSignal";
    }

    public static Jedis jedis()
    {
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
        }
        else
        {
            System.out.println("No Database");
        }
        return jedis;
    }

    public static void set(String key, String value)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        ///it's important to return the Jedis instance to the pool once you've finished using it
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            jedis.set(key, value);
            jedis.close();
        }
        else
        {
            System.out.println("No Database");
        }
    }

    public static String get(String key)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            String status = jedis.get(key);
            jedis.close();
            return status;
        }
        else
        {
            System.out.println("No Database");
        }
        return null;
    }

    public static void hashSet(String hashKey, String dataKey, String value)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            jedis.hset(hashKey, dataKey, value);
            jedis.close();
        }
        else
        {
            System.out.println("No Database");
        }
    }

    public static void hashmapSet(String key, Map<String, String> value)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            jedis.hmset(key, value);
            jedis.close();
        }
        else
        {
            System.out.println("No Database");
        }
    }

    public static List<String> hashmapGet(String key, String... fields)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            List<String> result = jedis.hmget(key, fields);
            jedis.close();
            return result;
        }
        else
        {
            System.out.println("No Database");
        }
        return null;
    }

    public static Map<String, String> hashmapGetAll(String key)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            Map<String, String> result = jedis.hgetAll(key);
            jedis.close();
            return result;
        }
        else
        {
            System.out.println("No Database");
        }
        return null;
    }

    public static long sortList_Length(String listName)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            Long result = jedis.zcard(listName);
            jedis.close();
            return result;
        }
        else
        {
            System.out.println("No Database");
        }
        return -1L;
    }

    public static boolean sortList_Add(String listName, String userId, long scoreValue)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            Long result = jedis.zadd(listName, scoreValue, userId);
            jedis.close();
            return (result == 0 || result == 1);
        }
        else
        {
            System.out.println("No Database");
        }
        return false;
    }

    public static boolean sortList_Add(String listName, String userId, int scoreValue)
    {
        return sortList_Add(listName, userId, (long) scoreValue);
    }

    public static boolean sortList_Remove(String listName, String userId)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            Long result = jedis.zrem(listName, userId);
            jedis.close();
            return result == 1;
        }
        else
        {
            System.out.println("No Database");
        }
        return false;
    }

    public static Long sortList_getRank(String listName, String userId)
    {
        return sortList_getRank(listName, userId, SORT_PARAM_DEC);
    }

    public static Long sortList_getRank(String listName, String userId, int orderParam) // orderParam : asc | dec
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            Long result = -1L;
            // Get map
            if (orderParam == SORT_PARAM_ASC)
            {
                result = jedis.zrank(listName, userId);
            }
            else
            {
                result = jedis.zrevrank(listName, userId);
            }
            jedis.close();
            return result;
        }
        else
        {
            System.out.println("No Database");
        }
        return -1L;
    }

    public static Set<String> sortList_getRange(String listName)
    {
        return sortList_getRange(listName, 0, -1, SORT_PARAM_DEC);
    }

    public static Set<String> sortList_getRange(String listName, int orderParam)
    {
        return sortList_getRange(listName, 0, -1, orderParam);
    }

    public static Set<String> sortList_getRange(String listName, long start, long end)
    {
        return sortList_getRange(listName, start, end, SORT_PARAM_DEC);
    }

    public static Set<String> sortList_getRange(String listName, long start, long end, int orderParam) // asc, dec
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            Set<String> result = null;
            if (orderParam == SORT_PARAM_ASC)
            {
                result = (Set<String>) jedis.zrange(listName, start, end);
            }
            else
            {
                result = (Set<String>) jedis.zrevrange(listName, start, end);
            }
            jedis.close();
            return result;
        }
        else
        {
            System.out.println("No Database");
        }
        return null;
    }

    public static Set<Tuple> sortList_getRangeWithScore(String listName, long start, long end)
    {
        return sortList_getRangeWithScore(listName, start, end, SORT_PARAM_DEC);
    }

    public static Set<Tuple> sortList_getRangeWithScore(String listName, long start, long end, int orderParam)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            Set<Tuple> result = null;
            if (orderParam == SORT_PARAM_ASC)
            {
                result = (Set<Tuple>) jedis.zrangeWithScores(listName, start, end);
            }
            else
            {
                result = (Set<Tuple>) jedis.zrevrangeWithScores(listName, start, end);
            }
            jedis.close();
            return result;
        }
        else
        {
            System.out.println("No Database");
        }
        return null;
    }

    public static boolean del(String key)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            boolean result = (jedis.del(key) == 1);
            jedis.close();
            return result;
        }
        else
        {
            System.out.println("No Database");
        }
        return false;
    }

    public static void expire(String key, int seconds)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            jedis.expire(key, seconds);
            jedis.close();
        }
        else
        {
            System.out.println("No Database");
        }
    }

    public static void persist(String key)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            jedis.persist(key);
            jedis.close();
        }
        else
        {
            System.out.println("No Database");
        }
    }

    public static Set<String> hkeys(String hashKey)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            Set<String> result = null;
            result = jedis.hkeys(hashKey);
            jedis.close();
            return result;
        }
        else
        {
            System.out.println("No Database");
        }
        return null;
    }

    public static long hdel(String hashKey, String key)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            Long result = -1L;
            result = jedis.hdel(hashKey, key);
            jedis.close();
            return result;
        }
        else
        {
            System.out.println("No Database");
        }
        return -1L;
    }

    public static Map<String, String> hgetAll(String hashKey)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            Map<String, String> result = null;
            result = jedis.hgetAll(hashKey);
            jedis.close();
            return result;
        }
        else
        {
            System.out.println("No Database");
        }
        return null;
    }

    public static boolean exists(String hashKey)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            boolean result = jedis.exists(hashKey);
            jedis.close();
            return result;
        }
        else
        {
            System.out.println("No Database");
        }
        return false;
    }

    public static Set<String> keys(String keys)
    {
        //get a jedis connection jedis connection pool
        Jedis jedis = jedisPool.getResource();
        if (jedis != null)
        {
            if (Main.dbIndex != 0)
            {
                jedis.select(Main.dbIndex);
            }
            Set<String> result = null;
            result = jedis.keys(keys);
            jedis.close();
            return result;
        }
        else
        {
            System.out.println("No Database");
        }
        return null;
    }
}
