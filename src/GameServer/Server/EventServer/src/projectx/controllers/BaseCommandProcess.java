package projectx.controllers;
/**
 * Created by hungbt
 */
public abstract class BaseCommandProcess {
    
    protected byte[] rawData;
    
    protected BaseCommandProcess(byte[] data) {
        rawData = data;
    }
    
    protected abstract byte[] process() throws Exception;
}