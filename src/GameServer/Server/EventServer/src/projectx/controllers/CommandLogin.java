package projectx.controllers;

import projectx.protocols.*;
import firebat.utils.Misc;
/**
 * Created by hungbt
 */
public class CommandLogin extends BaseCommandProcess {
    
    public CommandLogin(byte[] data) {
        super(data);
    }

    @Override
    public byte[] process() throws Exception {
        // get params
        LoginRequest lReq = LoginRequest.parseFrom(rawData);
        String platform = lReq.getPlatform();
        String buildVersion = lReq.getBuildVersion();
        // process...
        int retCode = CmdResult.SUCCESS.getNumber();
        String serverTime = Misc.getCurrentUtcTime();
        boolean isEnableGiftCode = true;
        boolean isEnableAds = true;
        // resp
        LoginResponse.Builder lRespBuilder = LoginResponse.newBuilder()
                                                            .setRetCode(retCode)
                                                            .setServerTime(serverTime)
                                                            .setEnableGiftCode(isEnableGiftCode)
                                                            .setEnableAds(isEnableAds);
        return lRespBuilder.build().toByteArray();
    }
}