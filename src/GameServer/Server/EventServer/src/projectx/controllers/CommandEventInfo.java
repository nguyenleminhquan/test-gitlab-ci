package projectx.controllers;

import firebat.utils.Misc;
import projectx.protocols.*;
import projectx.event.EventInfo;
import projectx.event.EventManager;
/**
 * Created by hungbt
 */
public class CommandEventInfo extends BaseCommandProcess {
    
    public CommandEventInfo(byte[] data) {
        super(data);
    }

    @Override
    public byte[] process() throws Exception {
        // process
        int retCode = CmdResult.SUCCESS.getNumber();
        String serverTime = Misc.getCurrentUtcTime();
        EventInfo eventInfo = EventManager.eventInfo != null ? EventManager.eventInfo : new EventInfo("", 0, 0);
        String eventId = eventInfo.eventId;
        String startTime = eventInfo.startTimeInString();
        int duration = eventInfo.durationInMinute;
        int breakTime = eventInfo.breakTimeInMinute;
        // resp
        EventInfoResponse.Builder lRespBuilder = EventInfoResponse.newBuilder()
                                                            .setRetCode(retCode)
                                                            .setServerTime(serverTime)
                                                            .setEventId(eventId)
                                                            .setStartTime(startTime)
                                                            .setDuration(duration)
                                                            .setBreakTime(breakTime);
        return lRespBuilder.build().toByteArray();
    }
}