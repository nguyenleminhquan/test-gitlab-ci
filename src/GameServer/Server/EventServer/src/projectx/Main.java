package projectx;

import firebat.io.http.HttpServer;
import firebat.log.Log;
import firebat.utils.*;
import projectx.database.Database;
import projectx.event.EventManager;
import projectx.io.GameServerInitializer;

/**
 * @author HuyNH4
 */
public class Main
{
    private final static ServiceStatus status = new ServiceStatus(true);
    public static Environment environment;
    public static HttpServer httpServer;
    public static int dbIndex = 0;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
            status.setStartingUp();
            addShutdownHook();
            int listenPort = 8091;
            String env = "";
            String idx = "";
            String host = "";
            String port = "";
            String pass = "";
            for(int i = 0; i < args.length; i++)
            {
                if(args[i].equals("-port"))
                    listenPort = Integer.parseInt(args[i+1]);
                if(args[i].equals("-environment"))
                    env = args[i+1];
                if(args[i].equals("-dbIndex"))
                    idx = args[i+1];
                if(args[i].equals("-dbHost"))
                    host = args[i+1];
                if(args[i].equals("-dbPort"))
                    port = args[i+1];
                if(args[i].equals("-dbPass"))
                    pass = args[i+1];
            }
            if(env == "")
                environment = Environment.DEV;
            else
            {
                if(env.equals("SERVER_LIVE"))
                    environment = Environment.SERVER_LIVE;
                else
                    environment = Environment.DEV;
            } 
            if(idx == "")
                dbIndex = 0;
            else
                dbIndex = Integer.parseInt(idx);
            Log.start(environment);
            Database.init(host, port, pass);
            EventManager.init();
			
            httpServer = new HttpServer();
            httpServer.start("*", listenPort, new GameServerInitializer());
			status.setStarted();

            System.out.println("["+String.valueOf(environment)+"] UTC Time : >>> " + Misc.getCurrentUtcTime());
            Log.debug("Start service...");
        }
        catch (Exception e)
        {
            Log.exception(e);
        }
    }

    public static synchronized void stop() throws Exception
    {
        status.setShuttingDown();
        if (httpServer != null)
            httpServer.stop();
        Log.stop();
        status.setShutdown();
    }

    private static void addShutdownHook()
    {
        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    Main.stop();
                }
                catch (Exception e)
                {
                    Log.exception(e);
                }
            }
        });
    }

    public static boolean isRunning()
    {
        return status.isStarted();
    }
}
