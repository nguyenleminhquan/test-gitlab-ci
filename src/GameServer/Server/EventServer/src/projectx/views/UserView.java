package projectx.views;

import projectx.io.GameServerHandler;

public class UserView {
    
    private GameServerHandler handler;
    
    public UserView(GameServerHandler hdler) {
        handler = hdler;
    }
    
    public String getClientIp() {
        return handler.getClientIp();
    }
    
    public void response(byte[] data) {
		handler.writeBinAndClose(data);
	}
    
    public void close () {
        handler.close();
    }
}