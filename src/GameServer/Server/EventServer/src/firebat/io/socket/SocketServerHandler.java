package firebat.io.socket;

import firebat.log.Log;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by thuanvt on 10/14/2014.
 */
public class SocketServerHandler extends ChannelInboundHandlerAdapter {
    private final static AtomicInteger numConnection = new AtomicInteger();

    private ChannelHandlerContext ctx;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        numConnection.incrementAndGet();
        Log.debug("[ACTIVE]", numConnection.get());
        this.ctx = ctx;
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        numConnection.decrementAndGet();
        Log.debug("[INACTIVE]", numConnection.get());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        writeAndFlush((ByteBuf) msg);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
        Log.exception(cause);
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            Log.debug("[IDLE]");
            ctx.close();
        }
    }

    public void removeIdleTime() {
        ChannelPipeline pipeline = ctx.pipeline();
        if (pipeline.get(SocketServerInitializer.PIPELINE_IDLE) != null)
            pipeline.remove(SocketServerInitializer.PIPELINE_IDLE);
    }

    public void setIdleTime(int idleTimeReader, int idleTimeWriter, int idleTimeAll) {
        removeIdleTime();
        ctx.pipeline().addFirst(SocketServerInitializer.PIPELINE_IDLE, new IdleStateHandler(idleTimeReader, idleTimeWriter, idleTimeAll, TimeUnit.MILLISECONDS));
    }

    public void writeAndFlush(ByteBuf data) {
        int len = data.readableBytes();
        ctx.writeAndFlush(ctx.alloc().buffer(len + SocketServerDecoder.HEADER_LEN)
                .writeInt(len)
                .writeBytes(data));
    }

    public void close() {
        ctx.close();
    }

    public static int getNumConnection() {
        return numConnection.get();
    }
}
