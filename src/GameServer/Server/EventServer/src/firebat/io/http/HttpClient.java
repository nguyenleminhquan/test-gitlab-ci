package firebat.io.http;

import firebat.log.Log;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.*;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import io.netty.util.CharsetUtil;

import javax.net.ssl.SSLException;
import java.net.InetSocketAddress;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by thuanvt on 11/11/2014.
 */
public class HttpClient {
    public static SslContext sslCtx;
    private static EventLoopGroup group = new NioEventLoopGroup();

    static {
        try {
            sslCtx = SslContext.newClientContext(InsecureTrustManagerFactory.INSTANCE);
        } catch (SSLException e) {
            Log.exception(e);
        }
        if (sslCtx == null) {
            try {
                sslCtx = SslContext.newClientContext(SslContext.defaultClientProvider());
            } catch (SSLException e) {
                Log.exception(e);
            }
        }
    }

    public synchronized static boolean shutdownLoopGroup() {
        return shutdownLoopGroup(100, 15000, TimeUnit.MILLISECONDS);
    }

    public synchronized static boolean shutdownLoopGroup(int quietPeriod, int timeout, TimeUnit unit) {
        if (group == null)
            return false;
        group.shutdownGracefully(quietPeriod, timeout, unit);
        group = null;
        return true;
    }

    public static boolean sendHttpRequest (EventLoopGroup eventLoopGroup,
                                           String urlSpec,
                                           HttpClientAbstractHandler handler,
                                           int connectTimeout,
                                           int idleTime) throws Exception {
        return sendHttpRequest(eventLoopGroup,
                urlSpec,
                HttpMethod.GET,
                null,
                null,
				null,
                handler,
                connectTimeout,
                idleTime,
                0,
                false
        );
    }

    public static boolean sendHttpRequest (EventLoopGroup eventLoopGroup,
                                           String urlSpec,
                                           HttpMethod method,
                                           HttpClientAbstractHandler handler,
                                           int connectTimeout,
                                           int idleTime,
                                           int maxContentLength,
                                           boolean useDecompress) throws Exception {
        return sendHttpRequest(eventLoopGroup,
                urlSpec,
                method,
                null,
                null,
				null,
                handler,
                connectTimeout,
                idleTime,
                maxContentLength,
                useDecompress
        );
    }

	public static boolean sendHttpRequest (EventLoopGroup eventLoopGroup,
                                           String urlSpec,
                                           HttpMethod method,
                                           String content,
                                           HttpClientAbstractHandler handler,
                                           int connectTimeout,
                                           int idleTime,
                                           int maxContentLength,
                                           boolean useDecompress) throws Exception {
        return sendHttpRequest(eventLoopGroup,
                urlSpec,
                method,
                Unpooled.copiedBuffer(content, CharsetUtil.UTF_8),
                "text/plain; charset=UTF-8",
				null,
                handler,
                connectTimeout,
                idleTime,
                maxContentLength,
                useDecompress
        );
    }

    public static boolean sendHttpRequest (EventLoopGroup eventLoopGroup,
                                           String urlSpec,
                                           HttpMethod method,
                                           byte[] content,
                                           HttpClientAbstractHandler handler,
                                           int connectTimeout,
                                           int idleTime,
                                           int maxContentLength,
                                           boolean useDecompress) throws Exception {
        return sendHttpRequest(eventLoopGroup,
                urlSpec,
                method,
                Unpooled.wrappedBuffer(content),
                "application/octet-stream",
				null,
                handler,
                connectTimeout,
                idleTime,
                maxContentLength,
                useDecompress
        );
    }
	
	
    /**
     *
     * @param eventLoopGroup eventLoopGroup xử lý request này. Nếu muốn request này xử lý cùng thread với connection gọi nó thì truyền ctx.channel().eventLoop() vào
     * @param urlSpec
     * @param method
     * @param content
     * @param contentType
     * @param handler
     * @param connectTimeout
     * @param idleTime
     * @param maxContentLength >0 thì sẽ thêm HttpObjectAggregator(maxContentLength) vào pipeline
     * @param useDecompress true thì thêm HttpContentDecompressor vào pipeline
     * @return
     * @throws Exception
     */
    public static boolean sendHttpRequest (EventLoopGroup eventLoopGroup,
                                        String urlSpec,
                                        HttpMethod method,
                                        ByteBuf content,
                                        String contentType,
                                        String authorization,
                                        HttpClientAbstractHandler handler,
                                        int connectTimeout,
                                        int idleTime,
                                        int maxContentLength,
                                        boolean useDecompress) throws Exception {
        URL url = new URL(urlSpec);
        boolean isHttps = url.getProtocol().equalsIgnoreCase("https");
        if (isHttps && sslCtx == null)
            return false;
        String uri = (url.getQuery() == null) ? url.getPath() : (url.getPath() + "?" + url.getQuery());
        int port = (url.getPort() < 0) ? url.getDefaultPort() : url.getPort();

        HttpRequest request;
        HttpHeaders headers;
        if (content == null) {
            request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, method, uri);
            headers = request.headers();
        } else {
            request = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, method, uri, content);
            headers = request.headers();
            headers.set(HttpHeaders.Names.CONTENT_LENGTH, content.readableBytes());
            headers.set(HttpHeaders.Names.CONTENT_TYPE, contentType);
			if(authorization!=null)
				headers.set(HttpHeaders.Names.AUTHORIZATION , authorization);
        }
        headers.set(HttpHeaders.Names.HOST, url.getHost());
        headers.set(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.CLOSE);
        headers.set(HttpHeaders.Names.CACHE_CONTROL, HttpHeaders.Values.NO_CACHE);
        if (useDecompress)
            headers.set(HttpHeaders.Names.ACCEPT_ENCODING, HttpHeaders.Values.GZIP);
        handler.request = request;

        new Bootstrap()
                .channel(NioSocketChannel.class)
                .option(ChannelOption.ALLOCATOR.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, connectTimeout)
                .group((eventLoopGroup == null) ? group : eventLoopGroup)
                .handler(new HttpClientInitializer(isHttps, idleTime, maxContentLength, useDecompress, handler))
                .connect(new InetSocketAddress(url.getHost(), port))
                .addListener(handler);

        return true;
    }
}
