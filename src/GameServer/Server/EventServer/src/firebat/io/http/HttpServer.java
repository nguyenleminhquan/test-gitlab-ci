package firebat.io.http;

import firebat.log.Log;
import firebat.utils.Address;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.util.concurrent.TimeUnit;

/**
 * Created by thuanvt on 10/14/2014.
 */
public class HttpServer {
    private String host;
    private int port;
    private ServerBootstrap bootstrap;
    private EventLoopGroup bossGroup, workerGroup;

    public HttpServer() {
        bootstrap = new ServerBootstrap()
                .channel(NioServerSocketChannel.class)
                .childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childOption(ChannelOption.TCP_NODELAY, true);
    }

    public ServerBootstrap getBootstrap() {
        return bootstrap;
    }

    public synchronized boolean start(String host, int port, ChannelInitializer<SocketChannel> initializer) throws Exception {
        if (bossGroup != null || workerGroup != null)
            return false;

        this.host = host;
        this.port = port;
        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();

        bootstrap.group(bossGroup, workerGroup)
                .childHandler(initializer)
                .bind(Address.getInetSocketAddress(host, port))
                .sync();

        Log.info("Start http server", host, port);
        return true;
    }


    public synchronized boolean stop() {
        return stop(100, 15000, TimeUnit.MILLISECONDS);
    }

    public synchronized boolean stop(int quietPeriod, int timeout, TimeUnit unit) {
        if (bossGroup == null || workerGroup == null)
            return false;

        Log.info("Stop http server", host, port);
        bossGroup.shutdownGracefully(quietPeriod, timeout, unit);
        bossGroup = null;

        workerGroup.shutdownGracefully(quietPeriod, timeout, unit);
        workerGroup = null;
        return true;
    }
}
