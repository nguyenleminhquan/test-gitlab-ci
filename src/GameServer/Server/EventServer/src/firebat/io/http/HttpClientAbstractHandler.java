package firebat.io.http;

import firebat.log.Log;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.HttpRequest;


/**
 * Created by thuanvt on 11/11/2014.
 */
public abstract class HttpClientAbstractHandler extends ChannelInboundHandlerAdapter implements ChannelFutureListener{
    public HttpRequest request;

    public abstract void connectFail ();

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ctx.writeAndFlush(request);
    }

    @Override
    public void operationComplete(ChannelFuture future) throws Exception {
        if (future.isDone() == false || future.isSuccess() == false)
            connectFail();
    }
}
