package firebat.io.http;

import firebat.log.Log;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMessage;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.util.CharsetUtil;

/**
 * Created by thuanvt on 11/11/2014.
 */
public class HttpClientHandler extends HttpClientAbstractHandler{
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        Log.debug("channelActive");
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        Log.debug("channelInactive");
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        Log.debug("channelRead");
        if (msg instanceof HttpMessage)
        {
            HttpMessage message = (HttpMessage) msg;
            if (HttpHeaders.is100ContinueExpected(message) || HttpHeaders.isTransferEncodingChunked(message))
            {
                ctx.close();
                return;
            }
            Log.debug("!!!Header!!!", message.toString());
        }
        if (msg instanceof HttpContent)
        {
            HttpContent content = (HttpContent) msg;
            Log.debug("!!!Content!!!", content.content().toString(CharsetUtil.UTF_8));

            if (content instanceof LastHttpContent) {
                ctx.close();
                Log.debug("!!!Content!!! --- LAST ---");
            }
        }
    }

    @Override
    public void connectFail() {
        Log.debug("connectFail");
    }
}
