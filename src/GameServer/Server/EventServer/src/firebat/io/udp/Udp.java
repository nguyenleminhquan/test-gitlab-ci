package firebat.io.udp;

import firebat.log.Log;
import firebat.utils.Address;
import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.util.CharsetUtil;

import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;

/**
 * Created by thuanvt on 11/18/2014.
 */
public class Udp {
    private String host;
    private int port;
    private Bootstrap bootstrap;
    private EventLoopGroup group;
    private Channel channel;

    private int sndBuf, rcvBuf;
    private int numThread;

    public Udp () {
        bootstrap = new Bootstrap()
                .channel(NioDatagramChannel.class)
                .option(ChannelOption.SO_BROADCAST, true)
                .option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
    }

    public Bootstrap getBootstrap() {
        return bootstrap;
    }


    public synchronized boolean start (String host, int port, ChannelInboundHandlerAdapter handler) throws Exception {
        if (group != null)
            return false;

        this.host = host;
        this.port = port;
        group = (numThread > 0) ? new NioEventLoopGroup(numThread) : new NioEventLoopGroup();

        if (sndBuf > 0)
            bootstrap.option(ChannelOption.SO_SNDBUF, sndBuf);
        if (rcvBuf > 0)
            bootstrap.option(ChannelOption.SO_RCVBUF, rcvBuf);

        channel = bootstrap.group(group)
                .handler(handler)
                .bind(Address.getInetSocketAddress(host, port))
                .sync()
                .channel();

        Log.info("Start udp", host, port);
        return true;
    }

    public synchronized boolean stop() {
        return stop(100, 15000, TimeUnit.MILLISECONDS);
    }

    public synchronized boolean stop(int quietPeriod, int timeout, TimeUnit unit) {
        if (group == null)
            return false;

        Log.info("Stop udp", host, port);
        channel = null;
        group.shutdownGracefully(quietPeriod, timeout, unit);
        group = null;

        return true;
    }

    public void write (InetSocketAddress address, CharSequence cs) {
        write(address, Unpooled.copiedBuffer(cs, CharsetUtil.UTF_8));
    }

    public void write (InetSocketAddress address, byte[] data) {
        write(address, Unpooled.wrappedBuffer(data));
    }

    public void write (InetSocketAddress address, ByteBuf buf) {
        if (channel != null) {
            channel.writeAndFlush(new DatagramPacket(buf, address));
        }
    }

    public Udp setBuf(int sndBuf, int rcvBuf) {
        this.sndBuf = sndBuf;
        this.rcvBuf = rcvBuf;
        return this;
    }

    public Udp setNumThread(int numThread) {
        this.numThread = numThread;
        return this;
    }
}
