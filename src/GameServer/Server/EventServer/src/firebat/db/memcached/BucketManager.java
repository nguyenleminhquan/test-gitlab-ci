package firebat.db.memcached;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import firebat.log.Log;
import firebat.utils.Environment;
import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class BucketManager
{   
    private final static String PROPERTY_LOCAL_FOLDER       = "localFolder";
    private final static String PROPERTY_BUCKET_NAME        = "bucketName";
    private final static String PROPERTY_BUCKET_TYPE        = "bucketType";
    private final static String PROPERTY_SERVER_IP          = "serverIp";
    private final static String PROPERTY_OPERATION_TIMEOUT  = "operationTimeOut";
    private final static String PROPERTY_COMPRESS_THRESHOLD = "compressThreshold";    

    private static final HashMap<String,AbstractDbKeyValue> mapBucket = new HashMap<>();
    
    public synchronized static void start (Environment environment) throws Exception
    {
        String folder = "./data/";
        String fileProperties;
        
        switch (environment)
        {
            case SERVER_LIVE:
                fileProperties = "memcachedServerLive.json";
                break;
            case SERVER_TEST:
                fileProperties = "memcachedServerTest.json";
                break;
            case DEV:
                fileProperties = "memcachedDev.json";
                break;
            default:
                throw new Exception("Not support environment " + environment.name());
        }
        
        start(folder + fileProperties);
    }
    
    public synchronized static void start (String filePath) throws Exception
    {   
        Log.info("Use " + filePath);
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(filePath), StandardCharsets.UTF_8))
        {
            JsonObject jsonObj = JsonParser.parseReader(reader).getAsJsonObject();
            for (Map.Entry<String,JsonElement> o : jsonObj.entrySet())
            {
                String bucketId = o.getKey();
                if (mapBucket.containsKey(bucketId))
                {
                    throw new Exception("Duplicate bucket id " + bucketId);
                }
                JsonObject info = o.getValue().getAsJsonObject();
                if (info.has(PROPERTY_LOCAL_FOLDER))
                {
                    String localFolder = info.get(PROPERTY_LOCAL_FOLDER).getAsString();
                    mapBucket.put(bucketId, new FileStore(localFolder));
                    Log.info("Memcached", bucketId, localFolder);
                }
                else
                {
                    String bucketType = info.get(PROPERTY_BUCKET_TYPE).getAsString().toUpperCase();
                    String bucketName = info.get(PROPERTY_BUCKET_NAME).getAsString();
                    String bucketUserName = System.getenv("COUCHBASE_USERNAME");
                    String bucketPassword = System.getenv("COUCHBASE_PASSWORD");
                    String bucketIP = System.getenv("COUCHBASE_IP");

                    switch (bucketType) {
                        case "MEMCACHE":
                            mapBucket.put(bucketId,
                                    new SpyMemcached(bucketIP, bucketName, bucketUserName, bucketPassword));
                            break;
                        case "MEMCACHED":
                            mapBucket.put(bucketId,
                                    new SpyMemcached(bucketIP, bucketName, bucketUserName, bucketPassword));
                            break;
                        case "COUCHBASE":
                            mapBucket.put(bucketId,
                                    new SpyMemcached(bucketIP, bucketName, bucketUserName, bucketPassword));
                            break;
                        default:
                            throw new Exception("Not support type: " + bucketType
                                    + ". Please use COUCHBASE or MEMCACHED or MEMCACHE.");
                    }
                    Log.info("Bucket", bucketId, bucketUserName, bucketType, bucketIP);
                }
            }
        }
        
        Log.info("Check database connection");
        int numRetry = 0;
        boolean connectSuccess = false;

        Thread.sleep(50);
        while ((connectSuccess == false) && (numRetry < 50))
        {
            connectSuccess = true;
            for (AbstractDbKeyValue db : mapBucket.values())
            {                
                if (db != null)
                {
                    com.couchbase.client.java.json.JsonObject testJsonValue = 
                        com.couchbase.client.java.json.JsonObject.create()
                            .put("Database Name", db.getName())
                            .put("Num retry", numRetry)
                            .put("Time create", System.currentTimeMillis() / 1000);

                    if (db.exist(AbstractDbKeyValue.TEST_KEY)) {
                        try {
                            DocumentTest testDocument = db.get(DocumentTest.class, AbstractDbKeyValue.TEST_KEY);
                            Log.debug("Last test database: " + testDocument.toString());
                        } catch (Exception e) {
                            Log.debug("Last test database error: ");
                            e.printStackTrace();
                        }
                        db.delete(AbstractDbKeyValue.TEST_KEY);
                    }

                    DocumentTest testDocument = Document.fromJson(DocumentTest.class, testJsonValue);
                    if (db.set(AbstractDbKeyValue.TEST_KEY, testDocument) == false)
                    {
                        connectSuccess = false;
                    }
                }
            }
            numRetry++;
            Thread.sleep(50);
        }

        if (connectSuccess == false)
        {
            throw new Exception("Connect database fail");
        }

        Log.info("Connect database success. Num retry is " + (numRetry - 1));
    }
    
    public synchronized static void stop ()
    {
        for (AbstractDbKeyValue db : mapBucket.values())
        {
            db.disconnect();
        }
    }
    
    public static AbstractDbKeyValue get (String bucketId)
    {
        return mapBucket.get(bucketId);
    }
    
    public static int getNumBucket ()
    {
        return mapBucket.size();
    }
}
