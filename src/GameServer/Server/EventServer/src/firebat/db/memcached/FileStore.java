package firebat.db.memcached;

import firebat.log.Log;
import firebat.utils.Time;

import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import com.couchbase.client.java.json.JsonObject;

public class FileStore extends AbstractDbKeyValue {
    private static final String FILE_SUFFIX = ".txt";
    private static final String EXPIRE_SUFFIX = "_expire";
    private static final String DEBUG_SUFFIX = "_debug";

    private String folder;
    private Path path;
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");

    public FileStore(String folder) {
        try {
            if (!folder.endsWith("/")) {
                folder += "/";
            }
            this.folder = folder;

            path = Paths.get(folder);

            if (Files.isDirectory(path) == false) {
                Files.createDirectories(path);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void disconnect() {
    }

    @Override
    public String getName() {
        return folder;
    }

    @Override
    public boolean exist(String key) {
        File file = new File(getKeyPath(key));
        return file.exists();
    }

    @Override
    public boolean set(String key, Document value) {
        return set(key, value, AbstractDbKeyValue.NO_EXPIRATION);
    }

    @Override
    public boolean set(String key, Document value, long expiration) {
        try {
            expiration = modifyExpiration(expiration);

            writeFile(getKeyPath(key), value.toString());
            writeFile(getKeyExpirePath(key, false), Long.toString(expiration));
            Date date = new Date(expiration * 1000);
            writeFile(getKeyExpirePath(key, true), dateFormat.format(date));
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public <T extends Document> T get(Class<T> type, String key) {
        try {
            if (!exist(key)) {
                Log.debug("Key not exist: " + key);
                return null;
            }

            long expire = Long.parseLong(readFile(getKeyExpirePath(key, false)));
            if (expire != 0 && expire < Time.currentTimeSecond()) {
                Log.debug("Key expired: " + key);
                delete(key);
                return null;
            }
            else 
            {
                String strValue = readFile(getKeyPath(key));
                T value = Document.fromJson(type, strValue);
                return value;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean delete(String key) {
        try {
            if (!exist(key)) {
                Log.debug("Key not exist: " + key);
                return false;
            }
            File valueFile = new File(getKeyPath(key));
            File expireFile = new File(getKeyExpirePath(key, false));
            File expireDebugFile = new File(getKeyExpirePath(key, true));
            return valueFile.delete() && expireFile.delete() && expireDebugFile.delete();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean replace(String key, Document value) {
        return replace(key, value, AbstractDbKeyValue.NO_EXPIRATION);
    }

    @Override
    public boolean replace(String key, Document value, long expiration) {
        return set(key, value, expiration);
    }

    private String getKeyPath(String key) {
        return folder + key + FILE_SUFFIX;
    }

    private String getKeyExpirePath(String key, boolean isDebugFile) {
        if (isDebugFile)
            return folder + key + EXPIRE_SUFFIX + DEBUG_SUFFIX + FILE_SUFFIX;
        else
            return folder + key + EXPIRE_SUFFIX + FILE_SUFFIX;
    }

    private String readFile(String path) throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader(path));
        String value = reader.readLine();
        reader.close();
        return value;
    }

    private void writeFile(String path, String value) throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        writer.write(value);
        writer.close();
    }

}
