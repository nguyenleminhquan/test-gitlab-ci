package firebat.db.jdbc;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;
import firebat.log.Log;
import firebat.utils.Environment;

import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class JdbcManager
{ 
    private final static String PROPERTY_DRIVER         = "driver";
    private final static String PROPERTY_URL            = "url";
    private final static String PROPERTY_USERNAME       = "username";
    private final static String PROPERTY_PASSWORD       = "password";
    private final static String PROPERTY_MINCONNECTION  = "minConnectionsPerPartition";
    private final static String PROPERTY_MAXCONNECTION  = "maxConnectionsPerPartition";
    private final static String PROPERTY_PARTITIONCOUNT = "partitionCount";
    
    private static final HashMap<String,BoneCP> mapDb = new HashMap<>();
    
    public synchronized static void start (Environment environment) throws Exception
    {
        String folder = "./data/";
        String fileProperties;
        
        switch (environment)
        {
            case SERVER_LIVE:
                fileProperties = "jdbcServerLive.json";
                break;
            case SERVER_TEST:
                fileProperties = "jdbcServerTest.json";
                break;
            case DEV:
                fileProperties = "jdbcDev.json";
                break;
            default:
                throw new Exception("Not support environment " + environment.name());
        }
        
        start(folder + fileProperties);
    }
    
    public synchronized static void start (String filePath) throws Exception
    {        
        Log.info("Use " + filePath);
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(filePath), StandardCharsets.UTF_8))
        {
            JsonObject jsonObj = JsonParser.parseReader(reader).getAsJsonObject();
            for (Map.Entry<String,JsonElement> o : jsonObj.entrySet())
            {
                String dbId = o.getKey();
                if (mapDb.containsKey(dbId))
                {
                    throw new Exception("Duplicate db id " + dbId);
                }
                JsonObject info = o.getValue().getAsJsonObject();
                
                Class.forName(info.get(PROPERTY_DRIVER).getAsString());
                
                BoneCPConfig config = new BoneCPConfig();
                config.setJdbcUrl(info.get(PROPERTY_URL).getAsString());
                config.setUsername(info.get(PROPERTY_USERNAME).getAsString());
                config.setPassword(info.get(PROPERTY_PASSWORD).getAsString());
                if (info.has(PROPERTY_MINCONNECTION))
                    config.setMinConnectionsPerPartition(info.get(PROPERTY_MINCONNECTION).getAsInt());
                if (info.has(PROPERTY_MAXCONNECTION))
                    config.setMinConnectionsPerPartition(info.get(PROPERTY_MAXCONNECTION).getAsInt());
                if (info.has(PROPERTY_PARTITIONCOUNT))
                    config.setPartitionCount(info.get(PROPERTY_PARTITIONCOUNT).getAsInt());
                
                BoneCP boneCP = new BoneCP(config);
                mapDb.put(dbId, boneCP);
                
                Log.info("JDBC", dbId, info.get(PROPERTY_URL).getAsString());
            }
        }
    }
    
    public synchronized static void stop ()
    {
        for (BoneCP boneCP : mapDb.values())
        {
            boneCP.shutdown();
        }
    }
    
    public static Connection getConnection (String dbId) throws SQLException
    {
        return mapDb.get(dbId).getConnection();
    }
    
    public static ResultSet executeQuery (String dbId, String sql, Object... params) throws SQLException
    {
        try (Connection conn = getConnection(dbId))
        {
            if (params.length == 0)
            {                
                return conn.createStatement().executeQuery(sql);
            }
            else
            {
                PreparedStatement ps = conn.prepareStatement(sql);
                int index = 1;
                for (Object obj : params)
                {
                    ps.setObject(index, obj);
                    index++;
                }
                return ps.executeQuery();
            }
        }
    }
    
    public static int executeUpdate (String dbId, String sql, Object... params) throws SQLException
    {
        try (Connection conn = getConnection(dbId))
        {
            if (params.length == 0)
            {                
                return conn.createStatement().executeUpdate(sql);
            }
            else
            {
                PreparedStatement ps = conn.prepareStatement(sql);
                int index = 1;
                for (Object obj : params)
                {
                    ps.setObject(index, obj);
                    index++;
                }
                return ps.executeUpdate();
            }
        }
    }
}
