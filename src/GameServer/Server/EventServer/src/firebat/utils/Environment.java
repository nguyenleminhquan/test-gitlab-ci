package firebat.utils;

public enum Environment
{
    SERVER_LIVE,
    SERVER_TEST,
    DEV;
}
