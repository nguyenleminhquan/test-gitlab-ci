package firebat.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Time
{    
    public final static int SECOND_IN_MINUTE        = 60;
    public final static int SECOND_IN_HOUR          = 60 * SECOND_IN_MINUTE;
    public final static int SECOND_IN_DAY           = 24 * SECOND_IN_HOUR;
    public final static int SECOND_IN_7_DAY         = 7 * SECOND_IN_DAY;
    public final static int SECOND_IN_30_DAY        = 30 * SECOND_IN_DAY;
    public final static int SECOND_IN_31_DAY        = 31 * SECOND_IN_DAY;
    
    public final static long MILLISECOND_IN_MINUTE   = 1000 * SECOND_IN_MINUTE;
    public final static long MILLISECOND_IN_HOUR     = 1000 * SECOND_IN_HOUR;
    public final static long MILLISECOND_IN_DAY      = 1000 * SECOND_IN_DAY;
    public final static long MILLISECOND_IN_7_DAY    = 1000 * SECOND_IN_7_DAY;
    public final static long MILLISECOND_IN_30_DAY   = 1000 * SECOND_IN_30_DAY;
    public final static long MILLISECOND_IN_31_DAY   = 1000 * SECOND_IN_31_DAY;
    
    /**
     * Lấy thời gian hiện tại theo đơn vị milli giây
     * @return
     */
    public static long currentTimeMillis ()
    {
        return System.currentTimeMillis();
    }
    
    /**
     * Lấy thời gian hiện tại theo đơn vị giây
     * @return
     */
    public static long currentTimeSecond ()
    {
        return System.currentTimeMillis() / 1000;
    }
    
    /**
     * Lấy thông tin thời gian hiện tại dạng chuỗi
     * @param pattern ví dụ: HH:mm:ss dd/MM/yyyy
     * @return
     */
    public static String currentDateString (String pattern)
    {
        return new SimpleDateFormat(pattern).format(new Date());
    }
    
    /**
     * Lấy thông tin thời gian dạng chuỗi
     * @param pattern ví dụ: HH:mm:ss dd/MM/yyyy
     * @param milliseconds thời cần chuyển về dạng chuỗi
     * @return
     */
    public static String getDateString (String pattern, long milliseconds)
    {
        return new SimpleDateFormat(pattern).format(new Date(milliseconds));
    }
}
