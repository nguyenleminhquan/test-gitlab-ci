package firebat.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Json
{
    public final static Gson gson = new GsonBuilder().create();    
    public final static Gson gsonPretty = new GsonBuilder().setPrettyPrinting().create();    
    
    public static <T> T fromJson (String json, Class<T> classOfT) throws Exception
    {
        return gson.fromJson(json, classOfT);
    }
    
    public static <T> T fromFile (String filePath, Class<T> classOfT) throws Exception
    {
        return gson.fromJson(Files.newBufferedReader(Paths.get(filePath), StandardCharsets.UTF_8), classOfT);
    }

    public static String toJson (Object src)
    {
        return gson.toJson(src);
    }    
    
    public static String toJsonPretty (Object src)
    {
        return gsonPretty.toJson(src);
    }
}
