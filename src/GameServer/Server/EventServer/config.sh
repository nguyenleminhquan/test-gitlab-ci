export BUILD_VERSION=$(cat ./version.txt)
export BS_CLIENT_CURRENT_VERSION=$(cat ./clientVersion.txt)

export JAR_NAME=LetsGetRich_Event_$BUILD_VERSION.jar
export MAIN_CLASS=projectx.Main

export ENVIRONMENT=$1

export PRJ_HOME=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
export DIR_DATA=$PRJ_HOME/../../ProtocolData
export DIR_LIB=$PRJ_HOME/lib
export DIR_SRC=$PRJ_HOME/src
export DIR_INFO=$PRJ_HOME/info
export DIR_TOOL=$PRJ_HOME/tools
export DIR_MPS_SRC_GEN=$DIR_SRC/projectx/protocols
export DIR_SRC_GAME=$DIR_SRC/projectx

LIBRARY=""
for lib in $(find $DIR_LIB -type f -name '*.jar')
do
  if [ "$lib" != "$DIR_LIB/jdbc/mysql-connector-java-5.1.28-bin.jar" ]; then
    if [ "$LIBRARY" = "" ]; then
      LIBRARY=$lib
    else
      LIBRARY=$LIBRARY:$lib
    fi
  fi
done
export LIBRARY=$LIBRARY

export DIR_TEMP=$PRJ_HOME/_temp_
export DIR_TEMP_SRC=$DIR_TEMP/0_src
export DIR_TEMP_CLASS=$DIR_TEMP/1_class

export DIR_RELEASE=$PRJ_HOME/_release_
export DIR_RELEASE_DATA=$DIR_RELEASE/data
export DIR_RELEASE_LIB=$DIR_RELEASE/lib

export JAVA=$JAVA_HOME/bin/java
export JAVAC=$JAVA_HOME/bin/javac
export JAR=$JAVA_HOME/bin/jar

export PROTO_COMPILER=/usr/bin/protoc
