#!/bin/bash

source ./config.sh $1

redis-server

cd $DIR_RELEASE
$JAVA -Xms16m -Xmx64m -jar $JAR_NAME -environment $ENVIRONMENT

cd $PRJ_HOME