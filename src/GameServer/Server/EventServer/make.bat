@echo off
setlocal ENABLEDELAYEDEXPANSION
cls
color 07


:CONFIG
echo.
echo Config ...
call config.bat

:: Verify folders
if NOT exist "%JAVA_HOME%" (
	echo JAVA_HOME variable [%JAVA_HOME%] is not correct JRE install folder!!!
	exit /b -1
)


set HAS_LIB=0
for %%I in (%LIBRARY%) do (
    set HAS_LIB=1
)
if /I "%1"=="src" (
    goto PREPROCESS
)


:CLEAN_DIR
echo.
echo Clean folder ...
if exist %DIR_TEMP_SRC% rd /s /q %DIR_TEMP_SRC%
if exist %DIR_TEMP_CLASS% rd /s /q %DIR_TEMP_CLASS%
if exist %DIR_TEMP% rd /s /q %DIR_TEMP%
if exist %DIR_RELEASE_LIB% rd /s /q %DIR_RELEASE_LIB%
if exist %DIR_RELEASE_DATA% rd /s /q %DIR_RELEASE_DATA%
if exist %DIR_RELEASE% rd /s /q %DIR_RELEASE%
if /I "%1"=="clean" (
    goto END
)


:CREATE_DIR
echo.
echo Create folder ...
if not exist %DIR_TEMP% md %DIR_TEMP%
if not exist %DIR_TEMP_SRC% md %DIR_TEMP_SRC%
if not exist %DIR_TEMP_CLASS% md %DIR_TEMP_CLASS%
if not exist %DIR_RELEASE% md %DIR_RELEASE%
if not exist %DIR_RELEASE_LIB% md %DIR_RELEASE_LIB%
if not exist %DIR_RELEASE_DATA% md %DIR_RELEASE_DATA%

echo cd..>%DIR_RELEASE%\make.bat
echo call make.bat>>%DIR_RELEASE%\make.bat
attrib +h %DIR_RELEASE%\make.bat

echo cd..>%DIR_RELEASE%\run.bat
echo call run.bat>>%DIR_RELEASE%\run.bat
attrib +h %DIR_RELEASE%\run.bat

:EXPORT_DATA
echo Export protocols ...
call export.bat

cd %PRJ_HOME%

:COPY_LIB
echo.
echo Copy lib ...
for %%I in (%LIBRARY%) do (
    copy /y %%I %DIR_RELEASE_LIB%\>nul 2>nul
)

:COPY_CONFIGS
echo.
echo copy data...
xcopy /Y /E %DIR_INFO%\%ENVIRONMENT% %DIR_RELEASE_DATA%>nul 2>nul

:PREPROCESS
echo.
echo Preprocess source ...
xcopy /Y /E %DIR_SRC% %DIR_TEMP_SRC%>nul 2>nul

:COMPILE
echo.
echo Compile source ...
cd %DIR_TEMP_SRC%
dir /s /b *.java>%DIR_TEMP%\listjava.txt
cd %DIR_TEMP%
if %HAS_LIB%==1 (
    %JAVAC% -Xlint:unchecked -Xlint:deprecation -encoding UTF-8 -classpath %LIBRARY% -d "%DIR_TEMP_CLASS%" @listjava.txt
) else (
    %JAVAC% -Xlint:unchecked -Xlint:deprecation -encoding UTF-8 -d "%DIR_TEMP_CLASS%" @listjava.txt
)
if ERRORLEVEL 1 goto ERROR


:MANIFEST
echo.
echo Create MANIFEST.MF ...
cd %DIR_TEMP%

echo Build-Version: %BUILD_VERSION%>MANIFEST.MF
echo Build-Time: %date% %time%>>MANIFEST.MF
echo Build-User: %username%>>MANIFEST.MF
echo Build-Computer: %COMPUTERNAME%>>MANIFEST.MF
set firstlib=netty-all-4.0.25.Final.jars

if %HAS_LIB%==1 (
    set LIBRARY=%LIBRARY:;=,%
    echo Class-Path: lib/%firstlib%>>MANIFEST.MF>>MANIFEST.MF
    for %%I in (%LIBRARY%) do (
        if %%~nxI==%firstlib% ( 
            echo.
        ) else ( 
            echo    lib/%%~nxI>>MANIFEST.MF
        )
    )
)
echo Main-Class: %MAIN_CLASS%>>MANIFEST.MF
echo !CLASS_PATH!

:JAR
echo.
echo Make jar...
cd %DIR_TEMP_CLASS%
%JAR% -cmf ..\MANIFEST.MF %DIR_RELEASE%\%JAR_NAME% .


goto END
:ERROR
color 4E
echo *************** HAVE ERROR ***************
rem Do not pause on CI agent
if "%AGENT_NAME%"=="" pause
color 07
exit /b -7


:END
echo.
echo Done.
cd %PRJ_HOME%
if /I "%1"=="run" (
    call run.bat
)