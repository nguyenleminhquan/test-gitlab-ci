#!/bin/bash

SERVICE_NAME="LetsGetRichEvent$ENV"

#----- RUN BY JAR -----
JAR_NAME=$(find ./ -type f -name LetsGetRich_Event*.jar | grep .)
RESULT=$?

if [ $RESULT = 0 ]; then
  JAR_NAME=${JAR_NAME: 2}
  SERVICE_ARGUMENT="-port $PORT -environment $ENV -dbHost $REDIS_HOST -dbPort $REDIS_PORT"
  START_CMD="java -jar $JAR_NAME $SERVICE_ARGUMENT $SERVICE_NAME"

  eval $START_CMD
else
  echo "No JAR file founded!" 1>&2
  exit 1
fi
