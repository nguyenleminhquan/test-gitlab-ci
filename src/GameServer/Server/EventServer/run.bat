@echo off
call config.bat

cd %REDIS_DATABASE_DIR%

start "LGRRedisDB" %REDIS_DATABASE_EXE%

timeout 2

cd %DIR_RELEASE%

%JAVA% -Xms16m -Xmx64m -jar %JAR_NAME% %ENVIRONMENT%

cd %PRJ_HOME%