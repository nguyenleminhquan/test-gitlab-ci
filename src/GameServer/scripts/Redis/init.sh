#!/bin/bash

export COMPOSE_PROJECT_NAME=lets-get-rich
# You can modify PROJECT_PATH depending on your machine
export PROJECT_PATH=$DATA_PATH/src
export LGR_NETWORK=lgr-live

export LGR_REDIS_COMPOSE=docker-compose.redis.yml
export LGR_REDIS_IMAGE=redis:4.0.11
export LGR_REDIS_BACKUP=$DATA_PATH/event-server/backup/redis
export LGR_REDIS_CONF=$DATA_PATH/event-server/config/redis