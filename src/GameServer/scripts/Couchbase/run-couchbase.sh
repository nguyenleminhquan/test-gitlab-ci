#!/bin/bash

cd /app

if [ -f "create-cluster.sh" ]; then
  bash ./create-cluster.sh
fi

for item in $(find ./ -type f -name '*.sh') 
do
  FILE_NAME=$(basename -- $item)
  if [ "$FILE_NAME" != "run-couchbase.sh" ] && [ "$FILE_NAME" != "create-cluster.sh" ] && [ "$FILE_NAME" != "init.sh" ]; then
    bash $item
  fi
done