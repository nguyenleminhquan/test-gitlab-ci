#!/bin/bash

couchbase-cli bucket-create -c localhost:8091 \
  --username $COUCHBASE_USERNAME \
  --password $COUCHBASE_PASSWORD \
  --bucket $COUCHBASE_BUCKET_MEMCACHED_NAME \
  --bucket-ramsize $COUCHBASE_BUCKET_MEMCACHED_RAMSIZE \
  --bucket-type $COUCHBASE_BUCKET_MEMCACHED_TYPE