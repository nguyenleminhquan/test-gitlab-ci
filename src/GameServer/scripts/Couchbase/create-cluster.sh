#!/bin/bash

couchbase-cli cluster-init -c 127.0.0.1 \
  --cluster-username $COUCHBASE_USERNAME \
  --cluster-password $COUCHBASE_PASSWORD \
  --services data,index,query \
  --cluster-ramsize $COUCHBASE_RAMSIZE \
  --cluster-index-ramsize $COUCHBASE_INDEX_RAMSIZE \
  --index-storage-setting default