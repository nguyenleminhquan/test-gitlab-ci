#!/bin/bash

export COMPOSE_PROJECT_NAME=lets-get-rich
# You can modify PROJECT_PATH depending on your machine
export PROJECT_PATH=$DATA_PATH/src
export LGR_NETWORK=lgr-live

export LGR_DB_COMPOSE=docker-compose.db.yml
export LGR_DB_IMAGE=couchbase:7.1.3
export LGR_DB_NAME=lgr-db
export LGR_DB_BACKUP=$DATA_PATH/backup/couchbase

export COUCHBASE_USERNAME=$1
export COUCHBASE_PASSWORD=$2
export COUCHBASE_NAME=letsgetrich
export COUCHBASE_RAMSIZE=4096
export COUCHBASE_INDEX_RAMSIZE=2048

export COUCHBASE_BUCKET_MEMCACHED_NAME=lgrcache
export COUCHBASE_BUCKET_MEMCACHED_RAMSIZE=2048
export COUCHBASE_BUCKET_MEMCACHED_TYPE=memcached

export COUCHBASE_BUCKET_NAME=lgrbase
export COUCHBASE_BUCKET_RAMSIZE=2048
export COUCHBASE_BUCKET_TYPE=couchbase
