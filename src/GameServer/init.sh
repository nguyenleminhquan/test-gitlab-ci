#!/bin/bash

export COMPOSE_PROJECT_NAME=lets-get-rich
# You can modify PROJECT_PATH depending on your machine
export PROJECT_PATH=$DATA_PATH/src
export LGR_NETWORK=lgr-live

export LGR_SERVER_COMPOSE=docker-compose.server.yml
export LGR_SERVER_IMAGE=hub.repo-mps.mto.zing.vn/mps/lets-get-rich-server
export LGR_SERVER_PORT=8000
export LGR_SERVER_ENV=SERVER_LIVE
export LGR_SERVER_NAME=lgr-server
export LGR_SERVER_LOG=$DATA_PATH/logs
export LGR_SERVER_TAG_LOCAL=latest

export LGR_EVENT_SERVER_COMPOSE=docker-compose.event-server.yml
export LGR_EVENT_SERVER_IMAGE=hub.repo-mps.mto.zing.vn/mps/lets-get-rich-event-server
export LGR_EVENT_SERVER_NAME=lgr-event-server
export LGR_EVENT_SERVER_TAG=latest
export LGR_EVENT_SERVER_PORT=8001
export LGR_EVENT_SERVER_ENV=SERVER_LIVE
export LGR_EVENT_SERVER_DATA=$DATA_PATH/event-server/data

export LGR_SERVER_TAG=$1

export COUCHBASE_PASSWORD=$2